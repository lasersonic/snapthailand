//
//  FBLoginViewController.swift
//  snapthailand
//
//  Created by Nutthawut on 4/6/17.
//  Copyright © 2017 Nutthawut. All rights reserved.
//

import FBSDKLoginKit

protocol FBLoginViewControllerDelegate : class {
    func loginSuccess(viewController: FBLoginViewController)
}

class FBLoginViewController : UIViewController{
  
  @IBOutlet weak var facebookLoginButton: FBSDKLoginButton!
  @IBOutlet weak var textLabel: UILabel!
    
    var delegate : FBLoginViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
        self.facebookLoginButton.readPermissions = ["public_profile", "email"]
        self.facebookLoginButton.delegate = self
    
        self.textLabel.text = "To get started.\nLog In using"
    }
  
    @IBAction func pressedSkip(_ sender: Any) {
      self.dismiss(animated: false, completion: nil)
    }
  
}

extension FBLoginViewController : FBSDKLoginButtonDelegate {
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
        if let error = error {
            self.showAlert("Error", error.localizedDescription)
            return
        }
        
        guard !result.isCancelled else {
            return
        }
        
        guard let token = result.token?.tokenString else {
            self.showAlert("Error", "Cannot retrieve Facebook authorization.")
            return
        }
        
        self.requestFacebookProfile(token : token)
        
    }
    
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
    }
    
}

//MARK: - Facebook / API Request.

extension FBLoginViewController {
    
    func requestFacebookProfile(token : String) {
        
        guard let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"first_name,last_name,birthday,email"], tokenString: token, version: nil, httpMethod: nil) else {
            return
        }
        
        graphRequest.start(completionHandler: { [weak self] (_, result, error) in

            guard let weakSelf = self else { return }

            if let error = error {
                weakSelf.showAlert("Error", error.localizedDescription)
                return
            }
            
            if let result = result as? [String:Any] {
                UserDefaults.standard.set(true, forKey: "isAlreadyLoginFacebook")
                let loginManager = FBSDKLoginManager()
                loginManager.logOut()

                weakSelf.delegate?.loginSuccess(viewController: weakSelf)

                weakSelf.saveFacebookUserToServer(result)
            }
        
        })
    }
    
    func saveFacebookUserToServer(_ result : [String: Any]) {
        
        guard let fbid = result["id"] as? String
            , let firstName = result["first_name"] as? String
            , let lastName = result["last_name"] as? String
            , let email = result["email"] as? String else {
                return
        }
        
        FacebookService.request(fbid, email: email, firstName: firstName, lastName: lastName, callback: { (success) in
            //Ignore error
        })
        
    }
}
