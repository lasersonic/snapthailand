//
//  StickerViewController.swift
//  snapthailand
//
//  Created by Nutthawut on 4/23/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit
import AVFoundation

class StickerViewController: BaseViewController {

    var image : UIImage!

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var label : THLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.imageView.image = image
        
        label = THLabel()
        label.text = "Add Text"
        label.textColor = UIColor.blue
        label.strokeSize = 3
        label.strokeColor = UIColor.orange
        
        let labelView = ZDStickerView(frame: CGRect(x: 100,y: 100,width: 180,height: 40))
        labelView.contentView = label
        labelView.translucencySticker = false
        labelView.preventsPositionOutsideSuperview = false
        labelView.showEditingHandles()
        labelView.stickerViewDelegate = self
        labelView.showCustomHandle()
        
        imageView.superview!.insertSubview(labelView, aboveSubview: imageView)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
//        let loc = touches.first?.locationInView(self.view)
//
//        if let view = touches.first?.view as? ZDStickerView {
//            if view.isEditingHandlesHidden() {
//                view.showEditingHandles()
//            }
//            else {
//                view.hideEditingHandles()
//            }
//        }
//        else {
            imageView.superview!.subviews.forEach { view in
                if let view = view as? ZDStickerView {
                    view.hideEditingHandles()
                }
            }
//        }

    }
    
    func addSticker(_ image : UIImage){
        let moon = UIImageView(image: image)
        let sticker = ZDStickerView(frame: CGRect(x: 100,y: 100,width: 180,height: 180))
        sticker.contentView = moon
        sticker.translucencySticker = false
        sticker.preventsPositionOutsideSuperview = false
        sticker.showEditingHandles()
        sticker.stickerViewDelegate = self
        imageView.superview!.insertSubview(sticker, aboveSubview: imageView)
    }
    
    func addFrame(_ image : UIImage){
        let frame = UIImageView(image: image)
        frame.frame = imageView.frame
        imageView.superview!.addSubview(frame)
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        let _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func nextAction(_ sender: AnyObject) {
        
        let rect = AVMakeRect(aspectRatio: self.image.size, insideRect: imageView.bounds)
        
        var rect2 = imageView.superview!.convert(rect, to: nil)
        rect2.origin.x = rect2.origin.x + 10
        rect2.origin.y = rect2.origin.y + 10
        
        UIGraphicsBeginImageContextWithOptions(rect2.size, true, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: -rect2.origin.x, y: -rect2.origin.y )
        self.view.layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as? ShareViewController
        vc?.image = image
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }

}

extension StickerViewController : ZDStickerViewDelegate {
    func stickerViewDidBeginEditing(_ sticker: ZDStickerView!) {
        sticker.showEditingHandles()
    }
    
    func stickerViewDidEndEditing(_ sticker: ZDStickerView!) {
    }
    
    func stickerViewDidCustomButtonTap(_ sticker: ZDStickerView!){
    }
    
    @IBAction func slideAction(_ sender: UISlider) {
        label.font = label.font.withSize(CGFloat(40 * sender.value))
    }
}

extension StickerViewController : UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StickerCell", for: indexPath)
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        let width = floor((collectionView.frame.width) / 5 - 4)
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.addSticker(UIImage(named: "thumbnail_shop")!)
    }
}
