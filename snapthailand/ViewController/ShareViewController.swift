//
//  ShareViewController.swift
//  snapthailand
//
//  Created by Nutthawut on 4/24/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit
import Social
import MessageUI
import Photos

import FacebookShare
import TwitterKit

struct FunctionItemTemplate {
    let name : String
    let icon : UIImage?
}
class ShareViewController: BaseViewController {


    fileprivate var documentController = UIDocumentInteractionController()
    fileprivate var collectionData : [FunctionItemTemplate]!
    fileprivate let recipients = ["info@snapthailand.rgb72.net"]
    fileprivate var fromIG = false
    
    var image : UIImage!

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionData =
        [ FunctionItemTemplate(name: "Share to facebook", icon: UIImage(named: "social-facebook"))
        ,FunctionItemTemplate(name: "Share to instagram", icon: UIImage(named: "social-instagram"))
        ,FunctionItemTemplate(name: "Share to twitter", icon: UIImage(named: "social-twitter"))
        ,FunctionItemTemplate(name: "Mail", icon: UIImage(named: "email"))
        ,FunctionItemTemplate(name: "Save", icon: UIImage(named: "save"))
        ,FunctionItemTemplate(name: "More", icon: UIImage(named: "more"))
        ]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeAction(_ sender: AnyObject) {
        
        UIView.animate(withDuration: 0.25, animations : {
            self.view.alpha = 0
        }, completion: { success in
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
            self.willMove(toParentViewController: nil)
        })

    }
    
    func mailAction(){
        if MFMailComposeViewController.canSendMail() {
            let data = UIImagePNGRepresentation(image)
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(recipients)
            mail.setMessageBody("#adventureearth", isHTML: true)
            mail.addAttachmentData(data!, mimeType: "image/png", fileName: "picture.png")
            self.present(mail, animated: true, completion: {
                AnswerHelper.logShareTo(.mail)
            })
        } else {
            showAlert("Error", message: "Share by email error.")
        }
    }
    
    func saveCameraRollAction(){
        UIImageWriteToSavedPhotosAlbum(self.image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil);
    }
    
    func facebookAction() {
      //  Old
//        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
//            AnswerHelper.logShareTo(.facebook)
//            let facebook = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
//            facebook?.setInitialText("#adventureearth")
//            facebook?.add(image)
//            self.present(facebook!, animated: true, completion: nil)
//        }
      
      //  New
      let photo = Photo(image: image, userGenerated: true)
      let hashtag = Hashtag.init("#adventureearth")
      var content = PhotoShareContent()
      content.photos = [photo]
      content.hashtag = hashtag

      do {
        try ShareDialog.show(from: self, content: content)
      } catch {
        print(error.localizedDescription)
      }
    }
    
    func twitterAction() {
      //  Old
//        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter) {
//            AnswerHelper.logShareTo(.twitter)
//            let twitter = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
//            twitter?.setInitialText("#adventureearth")
//            twitter?.add(image)
//            self.present(twitter!, animated: true, completion: nil)
//        }
      
      //  New
      let composer = TWTRComposer()
      composer.setText("#adventureearth")
      composer.setImage(image)
      composer.show(from: self) { (result) in
        if (result == .done) {
          print("Successfully composed Tweet")
        } else {
          print("Cancelled composing")
        }
      }
    }
    
    func showAlert(_ title:String?, message:String?){
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alertVC, animated: true, completion: nil)
    }
    
    
    
    @IBAction func backAction(_ sender: AnyObject) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func instagramAction() {
        
        let instagramURL = URL(string: "instagram://app")
        
        if UIApplication.shared.canOpenURL(instagramURL!) {
            
            fromIG = true
            
            UIImageWriteToSavedPhotosAlbum(self.image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil);
            
        }
        else {
            showAlert("Error", message: "Instagram isn't installed ")
        }
    }
    
    
    func etcAction() {
        
        AnswerHelper.logShareTo(.more)
        
        let captionString = "#adventureearth"
        
        let writePath = (NSTemporaryDirectory() as NSString).appendingPathComponent("img.jpg")
        
        if ((try? UIImageJPEGRepresentation(image!, 1.0)?.write(to: URL(fileURLWithPath: writePath), options: [.atomic])) != nil) == true{
            let fileURL = URL(fileURLWithPath: writePath)
            
            documentController = UIDocumentInteractionController(url: fileURL)
            documentController.uti = "public/jpeg"
            documentController.annotation = NSDictionary(object: captionString, forKey: "InstagramCaption" as NSCopying)
            documentController.presentOpenInMenu(from: self.view.frame, in: self.view, animated: true)
        }
    }

    //MARK:- Save Camera Roll Callback
    func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        
        if error == nil {
            if fromIG {
                fromIG = false
                
                let fetchOptions = PHFetchOptions()
                fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                if let lastAsset = fetchResult.firstObject{
                    let localIdentifier = lastAsset.localIdentifier
                    guard let mediaURL = URL(string: "instagram://library?LocalIdentifier=\(localIdentifier)") else { return }
                    AnswerHelper.logShareTo(.instagram)
                    UIApplication.shared.openURL(mediaURL)
                }
                
            }
            else {
                AnswerHelper.logShareTo(.cameraroll)
                showAlert("Success", message: "Image was saved to your camera roll.")
            }

        } else {
            showAlert("Error", message: error!.localizedDescription)
        }
        
    }
    
}

extension ShareViewController : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let shareCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ShareCell
        shareCell.text.text = collectionData[indexPath.row].name
        shareCell.icon.image = collectionData[indexPath.row].icon

        return shareCell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(1, 1, 1, 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (UIScreen.main.bounds.width-4) / 3
        let f =  floor(width)
        return CGSize(width: f, height: f)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.item == 0 {
            self.facebookAction()
        }
        else if indexPath.item == 1 {
            self.instagramAction()
        }
        else if indexPath.item == 2 {
            self.twitterAction()
        }
        else if indexPath.item == 3 {
            self.mailAction()
        }
        else if indexPath.item == 4 {
            self.saveCameraRollAction()
        }
        else if indexPath.item == 5 {
            self.etcAction()
        }
    }
}

extension ShareViewController : MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

class ShareCell : UICollectionViewCell {
    
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var icon: UIImageView!
}
