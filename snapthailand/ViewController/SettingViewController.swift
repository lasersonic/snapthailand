//
//  SettingViewController.swift
//  snapthailand
//
//  Created by Nutthawut on 5/31/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit
import MessageUI
import RealmSwift
import SwiftyStoreKit
import Async

class SettingViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let view = tableView.tableFooterView, let label = view.viewWithTag(99) as? UILabel {
            
            let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
            let build = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String

            label.text = "ADVENTURE EARTH Version \(version)(\(build)) \n©2016, Adventure Earth"
        }
        
    }

    @IBAction func backAction(_ sender: AnyObject) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
}

extension SettingViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Purchasing"
        }
        else if section == 1 {
            return "Other"
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell")!
        var text = ""
        cell.accessoryType = .none
        if indexPath.section == 0{
            if indexPath.row == 0 {
                text = "Sticker shop"
            }
            else {
                text = "Restore purchase"
            }
        }
        
        if indexPath.section == 1{
            if indexPath.row == 0 {
                text = "Send feedback"
            }
            else {
                text = "Rate this app"
            }
        }
        cell.textLabel?.text = text
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0 : goToStickerShop()
            break
            case 1 : restorePurchase()
            break
            default: break;
            }
        }
        else if indexPath.section == 1 {
            switch indexPath.row {
            case 0 :
                sendFeedback()
                break
            case 1 :
                rateThisApp()
                break
            default : break;
            }
        }
    }
    
    func goToStickerShop(){
        if let shopVC = self.storyboard?.instantiateViewController(withIdentifier: "ShopViewController") as? ShopViewController {
            
            self.navigationController?.pushViewController(shopVC, animated: true)
        }
    }
    
    func restorePurchase(){
        
        let vc = UIAlertController(title: "Restore", message: "This will restore your any purchased stickers from your Apple ID account.", preferredStyle: .alert)
        let actionYes = UIAlertAction(title: "Restore", style: .default) { (action) in
            self.doRestore()
        }
        let actionNo = UIAlertAction(title: "Cancel", style: .cancel){action in}
        
        vc.addAction(actionYes)
        vc.addAction(actionNo)

        self.present(vc, animated: true, completion: nil)
        
    }
    
    func sendFeedback(){

        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["support@rgb72.net"])
            mail.setMessageBody("", isHTML: true)
            mail.setSubject("Snapthailand Feedback")
            present(mail, animated: true, completion: nil)
        }

    }
    
    func rateThisApp(){
        let url = URL(string: "https://itunes.apple.com/th/app/adventure-earth/id1116651608")!
        UIApplication.shared.openURL(url)
    }
    
    func doRestore(){
        
        SwiftyStoreKit.restorePurchases { (results) in
            
            for fail in results.restoreFailedProducts {
                let error = fail.0 as NSError
                print(error.localizedDescription)
                self.showAlert("Error", error.localizedDescription)
                break
            }
            
            
            print(results.restoredProducts)
            
            Async.background {
                
                let realm = try! Realm()
                
                for restoreProduct in results.restoredProducts {
                    let purchased = PurchasedItem()
                    purchased.purchasedIds = restoreProduct.productId
                    
                    let countOfId = realm.objects(PurchasedItem.self)
                        .filter(NSPredicate(format: "purchasedIds == %@", restoreProduct.productId))
                        .count
                    
                    if countOfId == 0 {
                        try! realm.write {
                            realm.add(purchased)
                        }
                    }
                }
                
                
                for a in realm.objects(PurchasedItem.self) {
                    print(a.purchasedIds ?? "NO transaction purchased id")
                }
                
                Async.main{
                    self.showAlert("Success", "Restore success")
                }
            }
            

        }
    }
    
}



extension SettingViewController : MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

