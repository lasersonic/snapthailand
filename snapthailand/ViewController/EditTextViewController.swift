//
//  EditTextViewController.swift
//  snapthailand
//
//  Created by Nutthawut on 6/5/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit


protocol EditTextViewControllerDelegate : class {
    func didEditWithText(_ text:String, alignment : NSTextAlignment)
}

class EditTextViewController: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var toolbarBottomConstraint: NSLayoutConstraint!

    weak var delegate : EditTextViewControllerDelegate?
    var textAlignMent =  NSTextAlignment.left
    var userData : [AnyHashable: Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        if let text = userData?["text"] as? String {
            self.textView.text = text
        }
        if let alignment = userData?["alignment"] as? Int {
            self.textView.textAlignment = NSTextAlignment(rawValue: alignment)!
        }
        
        self.textView.becomeFirstResponder()
        
    }

    func keyboardWillShow(_ notification : Notification){
        
        let frame = notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let time = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber

        self.view.layoutIfNeeded()

        self.toolbarBottomConstraint.constant = frame.cgRectValue.height
        
        UIView.animate(withDuration: time.doubleValue, animations: {
            self.view.layoutIfNeeded()
            self.view.setNeedsLayout()
        }) 
    }
    
    func keyboardWillHide(_ notification : Notification){

        let time = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        
        self.view.layoutIfNeeded()
        
        self.toolbarBottomConstraint.constant = 0
        
        UIView.animate(withDuration: time.doubleValue, animations: {
            self.view.layoutIfNeeded()
            self.view.setNeedsLayout()
        }) 

    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(_ sender: AnyObject) {
        self.textView.resignFirstResponder()
        dismiss(animated: true, completion: nil)
    }

    @IBAction func okAction(_ sender: AnyObject) {
        self.textView.resignFirstResponder()
        delegate?.didEditWithText(self.textView.text, alignment: textAlignMent)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func leftAction(_ sender: AnyObject) {
        textAlignMent = NSTextAlignment.left
        self.textView.textAlignment = textAlignMent
    }
    
    @IBAction func centerAction(_ sender: AnyObject) {
        textAlignMent = NSTextAlignment.center
        self.textView.textAlignment = textAlignMent
    }
    
    @IBAction func rightAction(_ sender: AnyObject) {
        textAlignMent = NSTextAlignment.right
        self.textView.textAlignment = textAlignMent
    }

}
