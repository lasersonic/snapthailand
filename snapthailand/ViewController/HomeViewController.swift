//
//  ViewController.swift
//  snapthailand
//
//  Created by Appsynth on 3/28/2559 BE.
//  Copyright © 2559 Nutthawut. All rights reserved.
//

import UIKit
import AVFoundation
import Crashlytics // If using Answers with Crashlytics
import SwiftKeychainWrapper
import FBSDKLoginKit

let KeychainKey = "encrypt_key"
let DeviceIdKey = "ae.deviceId"

class HomeViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var backgroundNameLabel: UILabel!
    
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnLibrary: UIButton!
    
    var slideIndex = 0
    let imageCount = 5

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupVerticalImageButton(self.btnCamera)
        self.setupVerticalImageButton(self.btnLibrary)
        
        self.prepareKeyChain()
        self.prepareDeviceID()

        //Show FB If need
        self.showFacebookLogin()
        self.setupScrollableImage()
    }
    
    
    //MARK: - Life Cycle
    
    deinit {
        print("close HomeViewController")
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    //MARK: - Other Methods
    
    private func showFacebookLogin() {
        let isAlreadyLoginFacebook = UserDefaults.standard.bool(forKey: "isAlreadyLoginFacebook")
      
        if !isAlreadyLoginFacebook {
          if let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "FBLoginViewController") as? FBLoginViewController {
            loginVC.delegate = self
            loginVC.modalPresentationStyle = .overCurrentContext
            self.present(loginVC, animated: false, completion: nil)
          }
        }
    }
    
    fileprivate func prepareKeyChain(){
        
        guard let _ = KeychainWrapper.standard.string(forKey: KeychainKey) else {
            let password = randomStringWithLength(14)
            KeychainWrapper.standard.set(password, forKey: KeychainKey)
            return
        }
    }
    
    fileprivate func prepareDeviceID() {
        
        guard let _ = KeychainWrapper.standard.string(forKey: DeviceIdKey) else {
            if let device_id = UIDevice.current.identifierForVendor?.uuidString {
                KeychainWrapper.standard.set(device_id, forKey: DeviceIdKey)
            }
            return
        }
    }
    
    fileprivate func setupScrollableImage(){
        collectionView.isUserInteractionEnabled = false
        collectionView.dataSource = self
        collectionView.delegate = self
        
        self.pageControl.numberOfPages = imageCount
        
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(slideImage), userInfo: nil, repeats: true)

    }
    
    
    func slideImage() {
        let index = slideIndex % (imageCount + 1)
        slideToIndex(index : index, anim : true)
        slideIndex += 1
    }
    
    func slideToIndex(index : Int, anim : Bool) {
        self.collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .left, animated: anim)
        switch index {
        case 0: self.backgroundNameLabel.text = "Bangkok Street"
        case 1: self.backgroundNameLabel.text = "Chiang Mai Night"
        case 2: self.backgroundNameLabel.text = "Peak of Samui Island"
        case 3: self.backgroundNameLabel.text = "Phangan, waiting for full moon party"
        default: self.backgroundNameLabel.text = "Exotic Phi Phi Island"
        }
    }
    
    fileprivate func setupVerticalImageButton(_ button: UIButton) {
        let spacing: CGFloat = 6.0
        let imageSize: CGSize = button.imageView!.image!.size
        button.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0);
        let labelString = NSString(string: button.currentTitle!)
        let titleSize = labelString.size(attributes: [NSFontAttributeName: button.titleLabel!.font])
        button.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width);
    }

    
    @IBAction func photoLibraryAction(_ sender: AnyObject) {
        
        let vc = FusumaViewController()
        vc.delegate = self
        
        let nav = UINavigationController()
        nav.viewControllers = [vc]
        
        self.present(nav, animated: true, completion: nil)
        
    }
    
    fileprivate func randomStringWithLength (_ len : Int) -> String {
        
        let letters : String = "abcdefghkmnopqrstuvxyzABCDEFGHKLMNOPQRSTUXYZ0123456789"
        var randomString = ""
        
        for _ in 0..<len{
            let length = UInt32(letters.characters.count)
            let rand = Int(arc4random_uniform(length))
            let char = letters[letters.characters.index(letters.startIndex, offsetBy: rand)]
            
            randomString.append(char)
        }
        return randomString
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "cameraAction" {
            Answers.logCustomEvent(withName: "User's image source", customAttributes: ["source":"camera"])
        }
    }
}

extension HomeViewController : FusumaDelegate {
    
    func fusumaCameraRollUnauthorized() {
        
    }
}


extension HomeViewController : FBLoginViewControllerDelegate {
    
    func loginSuccess(viewController : FBLoginViewController) {
        viewController.dismiss(animated: false, completion: nil)
    }
}


extension HomeViewController : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return UIScreen.main.bounds.size
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageCount + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        guard let imageView = cell.contentView.viewWithTag(100) as? UIImageView else {
            return cell
        }
        //should be imageview
        
        let i = (indexPath.row % imageCount) + 1
        
        imageView.image = UIImage(named: "bg-\( i )")
        return cell
    }
}


extension HomeViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = Int(round(scrollView.contentOffset.x / scrollView.frame.width))
        pageControl.currentPage = page % imageCount
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        let page = Int(round(scrollView.contentOffset.x / scrollView.frame.width))
        if page == imageCount {
            slideToIndex(index : 0, anim : false)
            slideIndex += 1
        }
    }
    
    
}
