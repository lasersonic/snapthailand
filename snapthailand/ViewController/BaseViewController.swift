//
//  BaseViewController.swift
//  snapthailand
//
//  Created by Appsynth on 4/19/2559 BE.
//  Copyright © 2559 Nutthawut. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    open override var shouldAutorotate : Bool {
        return false
    }
    
    open override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if let topView = self.topViewController {
            return topView.supportedInterfaceOrientations
        }
        return .portrait
    }

}

class BaseViewController: UIViewController {

    @IBOutlet weak var bannerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
