//
//  ShopViewController.swift
//  snapthailand
//
//  Created by Nutthawut on 6/4/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit
import StoreKit
import RealmSwift
import SwiftyStoreKit

protocol ShopViewControllerDelegate: class {
    func reload()
}

protocol ShopViewControllerFilterDelegate: class {
    func backFromShopAction()
}

class ShopViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var stickers : [StickerItem]?
    var purchasedItems : [PurchasedItem] = []
    var downloadedItems : [LocalSticker] = []
    
    var delegate : ShopViewControllerFilterDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let realm = try! Realm()
        purchasedItems = Array(realm.objects(PurchasedItem.self))
        downloadedItems = Array(realm.objects(LocalSticker.self))
        
        GetStickerService.requestShop { stickers in
            
            self.stickers = [StickerItem]()
            
            if let stickers = stickers{
                
                var infos = [String]()
                
                for s in stickers {
                    infos.append(s.inapp_id!)
                }
                
                let infoSet = Set<String>(infos)
                
                SwiftyStoreKit.retrieveProductsInfo(infoSet) { (result) in
                    
                    MBProgressHUD.hide(for: self.view, animated: true)

                    for product in result.retrievedProducts {
                        
                        let formatter = NumberFormatter()
                        formatter.locale = product.priceLocale
                        formatter.numberStyle = .currency
                        var priceString = product.price == 0 ? "Free" : formatter.string(from: product.price)!
                                                
                        let array = self.purchasedItems.filter{$0.purchasedIds == product.productIdentifier}
                        if array.count == 1 {
                            priceString = "Download"
                        }

                        print("Product: \(product.localizedDescription), price: \(priceString)")
                        
                        self.stickers = stickers.map({ (sticker) -> StickerItem in
                            if sticker.inapp_id! == product.productIdentifier {
                                sticker.price = priceString
                            }
                            
                            return sticker
                        })
                    }
                    
                    if let invalidProductId = result.invalidProductIDs.first {
                        return print("Invalid product identifier: \(invalidProductId)")
                    }
                    
                    self.tableView.reloadData()

                }
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        if let nav = self.navigationController {
            if let viewcontrollers = self.navigationController?.viewControllers {
                if viewcontrollers.count > 1 {
                    nav.popViewController(animated: true)
                    return
                }
            }
        }

        self.dismiss(animated: true, completion: nil)
        
        delegate?.backFromShopAction()
    }
    
    func purchaseAction(_ button : UIButton) {

        guard let sticker = self.stickers?[button.tag] else {
            return
        }
        
        guard let stickerId = sticker.sticker_id else {
            return
        }
        
        if sticker.price.lowercased() == "free" || sticker.price.lowercased() == "download" {
            self.downloadSticker(sticker)
            return
        }
        
        if !SwiftyStoreKit.canMakePayments {
            self.showAlert("Error", "Cannot make payment right now, please try again later")
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        if let inapp_id = sticker.inapp_id {
            
            SwiftyStoreKit.purchaseProduct(inapp_id) {[weak self] (result) in
                
                guard let strongSelf = self else { return }

                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                
                switch result {
                case .success( let product ) :
                    print("product id was buying...")
                    
                    AnswerHelper.logStickerBuying(product.productId)

                    SendOrderPurchaseService.request( stickerId ) {[weak self] (success) in
                        
                        guard let strongSelf = self else { return }
                        
                        if success {
                            strongSelf.downloadSticker(sticker)
                        }
                    }
                    
                    break
                    
                case .error(let error):
                    
                    strongSelf.showAlert("Error", error.localizedDescription)
                    
                    
                    break
                }
            }
        }
    }
    
    func downloadSticker( _ sticker : StickerItem){
        
        let progressView = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressView?.mode = .annularDeterminate
        progressView?.labelText = "Downloading"
        
        DownloadManager.sharedInstance.download(sticker, progressBlock: { progress in
            
            DispatchQueue.main.async(execute: {
                progressView?.progress = progress
            })
            
            }, successBlock:  {
                progressView?.hide(true)
                
                for (index,s) in self.stickers!.enumerated() {
                    if s.inapp_id == sticker.inapp_id {
                        let stickerCell = self.tableView.cellForRow(at: IndexPath(item: index, section: 0)) as! StickerCell
                        stickerCell.btnBuy.isHidden = true
                        break
                    }
                }
        })
    }

}

extension ShopViewController : UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let s = stickers {
            return s.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let stickerCell = tableView.dequeueReusableCell(withIdentifier: "StickerCell") as! StickerCell
        let sticker = stickers![indexPath.row]
        
        stickerCell.lblTitle.text = sticker.name
        stickerCell.lblSubtitle.text = String(format:"%d Images",sticker.stickerLists!.count)
        stickerCell.btnBuy.setTitle(sticker.price, for: UIControlState())
        stickerCell.btnBuy.addTarget(self, action: #selector(purchaseAction(_:)), for: .touchUpInside)
        stickerCell.btnBuy.tag = indexPath.row
        
        
        let ar = downloadedItems.filter{ $0.stickerId == sticker.inapp_id }
        if ar.count > 0 {
            stickerCell.btnBuy.isHidden = true
        }
        else {
            stickerCell.btnBuy.isHidden = false
        }
        
        stickerCell.thumbmain.sd_setImage(with: URL(string: sticker.avatar!)!)

        
        if let previews = sticker.preview_thumbnails {
            for i in 0 ..< min(2, previews.count) {
                let imageView = stickerCell.thumbnailCollection[i]
                if let url = URL(string: previews[i]) {
                    imageView.sd_setImage(with: url)
                }
            }
        }
        
        return stickerCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let shopVC = self.storyboard?.instantiateViewController(withIdentifier: "ShopDetailViewController") as? ShopDetailViewController {
            let sticker = stickers![indexPath.row]
            shopVC.sticker = sticker
            shopVC.delegate = self
            self.navigationController?.pushViewController(shopVC, animated: true)
        }

    }
}

extension ShopViewController : ShopViewControllerDelegate {
    func reload() {
        let realm = try! Realm()
        purchasedItems = Array(realm.objects(PurchasedItem.self))
        downloadedItems = Array(realm.objects(LocalSticker.self))

        self.tableView.reloadData()
    }
}
