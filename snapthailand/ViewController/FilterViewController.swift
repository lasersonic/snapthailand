//
//  FilterViewController.swift
//  snapthailand
//
//  Created by Appsynth on 3/29/2559 BE.
//  Copyright © 2559 Nutthawut. All rights reserved.
//

import UIKit
import CoreLocation
import RealmSwift
import Photos
import Async
import SwiftKeychainWrapper
import RNCryptor
import GPUImage

enum SlideAction : String {
    case crop
    case rotate
    case slideFilterIntensity
    case slideShapen
    case slideWhite
    case slideContrast
    case slideBrightness
    case slideSaturation
    case slideAdjustFontOpacity
}

enum DecorateMenu {
    case filter
    case effect
    case frame
    case text
    case stickerCategory
    case sticker
    case location
}

enum AdjustMenu {
    case adjustFont
    case adjustFontColor
}


enum AdjustCollectionMode {
    case progress
    case okCancel
    case rotate
    case collection
}

let LOCATION_SELECTOR_TAG = 12

class FilterViewController: BaseViewController {

    //MARK:- Public variables
    var image : UIImage?
    
    //MARK:- Private Variables
    @IBOutlet fileprivate weak var imageViewBackground: UIImageView!
    @IBOutlet fileprivate weak var btnFilter: UIButton!
    @IBOutlet fileprivate weak var btnEffect: UIButton!
    @IBOutlet fileprivate weak var btnFrame: UIButton!
    @IBOutlet fileprivate weak var btnText: UIButton!
    @IBOutlet fileprivate weak var btnSticker: UIButton!
    @IBOutlet fileprivate weak var btnLocation: UIButton!

    @IBOutlet fileprivate weak var menuConstraint: NSLayoutConstraint!
    
    @IBOutlet fileprivate weak var adjustBar: UIView!
    @IBOutlet fileprivate weak var correctWrongView: UIView!
    @IBOutlet fileprivate weak var filterBar: UIView!
    @IBOutlet fileprivate weak var btnOKAdjust: UIButton!
    @IBOutlet fileprivate weak var btnCancelAdjust: UIButton!
    
    @IBOutlet fileprivate weak var btnCropCancel: UIButton!
    @IBOutlet fileprivate weak var btnCropOK: UIButton!
    
    fileprivate var tempFilter : [UIImage]?
    fileprivate var filterTemplates : [PlistFilterItem]!
    fileprivate var effectTemplates : [PlistItem]!
    fileprivate var textTemplates : [PlistItem]!
    fileprivate var menuTemplates : [PlistItem]!
    fileprivate var frameTemplates : [PlistFrameItem]!
    fileprivate var fontsList : [PlistFontItem]!
    
    @IBOutlet fileprivate weak var collectionViewMenu: UICollectionView!
    @IBOutlet fileprivate weak var collectionAdjust: UICollectionView!
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    @IBOutlet fileprivate weak var cropView: AKImageCropperView!
    
    @IBOutlet fileprivate weak var lblAdjustTitle: UILabel!
    @IBOutlet fileprivate weak var adjustProgressBarView: UIView!
    fileprivate var adjustProgressBar: NTProgressBar!
    
    fileprivate var intensity : CGFloat = 0
    fileprivate var brightnessLevel : CGFloat = 0
    fileprivate var contrastLevel : CGFloat = 1
    fileprivate var saturationLevel : CGFloat = 1
    fileprivate var sharpenLevel : CGFloat = 0
    fileprivate var whiteLevel : CGFloat = 1
    
    fileprivate var currentFilter : Filter?
    fileprivate var currentSlideAction :  SlideAction?
    fileprivate var currentMenu : DecorateMenu?
    fileprivate var currentAdjustMenu : AdjustMenu? {
        didSet {
            if currentAdjustMenu == nil {
                
                guard let selectedItems = collectionView.indexPathsForSelectedItems else  { return }
        
                for indexPath in selectedItems {
                    collectionView.deselectItem(at: indexPath, animated: true)
                }
            }
        }
    }
    
    deinit {
        print("close FilterViewController")
    }
    
    weak var currentEditingTextSticker : NTStickerView? {
        didSet {
            
            if currentAdjustMenu != nil {
                self.removeStickerCommandView()
                return
            }
            
            if currentEditingTextSticker == nil {
                self.removeStickerCommandView()
            } else {
                self.addStickerCommandView()
            }
            
            if currentMenu == .text {
                
                if let cell = self.collectionView.cellForItem(at: IndexPath(item: 1, section: 0)) as? FilterCollectionCell {
                    cell.enableButton(currentEditingTextSticker != nil )
                }
                if let cell = self.collectionView.cellForItem(at: IndexPath(item: 2, section: 0)) as? FilterCollectionCell {
                    cell.enableButton(currentEditingTextSticker != nil )
                }
            }
        }
    }
    
    fileprivate var scrollviewLocation : UIScrollView!
    fileprivate var pageControlLocation : UIPageControl?
    fileprivate var locationScrollPage : Int = 0
    fileprivate weak var viewLocation :LocationView?
    fileprivate var commandView : UIView?

    fileprivate var imageRotateDegree : (finalDegree : CGFloat, tempDegree : CGFloat) = (0 , 0)

    fileprivate var frame : UIImageView?
    
    fileprivate var stickerLists = [LocalStickerInstance]()
    
    fileprivate var colorList : [UIColor]{
        return [
            UIColor(r: 0, g: 0, b: 0),
            UIColor(r: 255, g: 255, b: 255),
            UIColor(r: 255, g: 0, b: 0),
            UIColor(r: 0, g: 255, b: 0),
            UIColor(r: 0, g: 0, b: 255),
            UIColor(r: 255, g: 255, b: 0),
            UIColor(r: 0, g: 255, b: 255),
            UIColor(r: 255, g: 0, b: 255),
            UIColor(r: 192, g: 192, b: 192),
            UIColor(r: 128, g: 128, b: 128),
            UIColor(r: 255, g: 140, b: 0),
            UIColor(r: 0, g: 100, b: 0),
            UIColor(r: 0, g: 128, b: 128),
            UIColor(r: 244, g: 164, b: 96),
            UIColor(r: 255, g: 218, b: 185),
            UIColor(r: 255, g: 255, b: 240),
            UIColor(r: 30, g: 144, b: 255),
            UIColor(r: 148, g: 0, b: 211),
            UIColor(r: 255, g: 105, b: 180),
            UIColor(r: 128, g: 0, b: 0),
            UIColor(r: 255, g: 192, b: 203),
            UIColor(r: 64, g: 224, b: 208),
            UIColor(r: 255, g: 215, b: 0),
            UIColor(r: 0, g: 0, b: 218),
        ]
    }
    
    fileprivate var currentCategorySelectIndex : Int = 0
    fileprivate var progressLabelView : UIView!
    fileprivate var progressBarCurrent = [SlideAction : CGFloat]()
    fileprivate var filterValueCurrent = [SlideAction : CGFloat]()
    fileprivate var valueProgress : CGFloat = 0
    fileprivate var realValueProgress : CGFloat = 0
    fileprivate var editingStatus : (isCropped : Bool, isRotated : Bool) = (false, false)
    

    var gotLocation = false
    var location : CLLocation?
    

    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initEffectProgressView()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(cropViewTapGestureAction(_:)))
        cropView.addGestureRecognizer(tapGesture)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        imageViewBackground.image = image
        
        if let plist = PlistHelper.loadPlist() {
            filterTemplates = plist.filter
            effectTemplates = plist.effect
            textTemplates = plist.text
            menuTemplates = plist.menu
            frameTemplates = plist.frame
            fontsList = plist.fonts
        }
        
        Async.background{ [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.tempFilter = [UIImage]()

            if let resizeImage = strongSelf.image?.resize() {
                
                for filterTemplate in strongSelf.filterTemplates {
                    
                    let filter = filterTemplate.filterTemplate.lookupFilter
                    
                    FilterHelper.filter(originalImage: resizeImage, filter: filter, intensity: 0.5, callback: { (filteredImage) in
                        self?.tempFilter?.append(filteredImage)
                    })
                    
                }
            }

        }.main {
            self.collectionViewMenu.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: UICollectionViewScrollPosition())
            self.btnFilterAction()
        }
        
        filterValueCurrent[SlideAction.slideFilterIntensity] = 0.5
        filterValueCurrent[SlideAction.slideBrightness] = 0
        filterValueCurrent[SlideAction.slideContrast] = 1
        filterValueCurrent[SlideAction.slideSaturation] = 1
        filterValueCurrent[SlideAction.slideShapen] = 0
        filterValueCurrent[SlideAction.slideWhite] = 1
        filterValueCurrent[SlideAction.slideAdjustFontOpacity] = 1

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let image = image, cropView.image == nil{
            cropView.image = image
        }
    }
    
    func initEffectProgressView() {
        
        let progressBound = adjustProgressBarView.bounds
        let bound = CGRect(x:0, y:0, width: UIScreen.main.bounds.width, height: progressBound.height)
        adjustProgressBar = NTProgressBar(frame: bound.insetBy(dx: 10, dy: 0))
        adjustProgressBar.delegate = self
        adjustProgressBarView.addSubview(adjustProgressBar)
        
        progressLabelView = UIView(frame: CGRect(x: 0,y: 0,width: 20,height: 14))
        let bubbleLayer = CAShapeLayer()
        bubbleLayer.path = bubblePathForContentSize(self.progressLabelView.bounds.size).cgPath
        bubbleLayer.fillColor = ColorHelper.selectedMenuColor?.cgColor
        progressLabelView.layer.addSublayer(bubbleLayer)
        
        let label = UILabel(frame: progressLabelView.bounds)
        label.tag = 99
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 10)
        label.text = "100"
        progressLabelView.addSubview(label)
        progressLabelView.isHidden = true
        self.view.addSubview(progressLabelView)
    }
    
    func defaultState(){
        
        for indexPath in self.collectionViewMenu.indexPathsForSelectedItems! {
            self.collectionViewMenu.deselectItem(at: indexPath, animated: false)
        }
        
        self.currentMenu = nil
        self.currentSlideAction = nil
        self.currentAdjustMenu = nil
        
        showMenu(false)
        self.adjustBar.alpha = 0
    }
    
    func loadLocalSticker( _ block : @escaping ()->Void ){
        
        Async.background{ [weak self] in
            
            guard let strongSelf = self else { return }
            
            let realm = try! Realm()
            let queryResult = realm.objects(LocalSticker.self).sorted(byKeyPath: "stickerOrder", ascending: true)
            for localSticker in queryResult {
                strongSelf.stickerLists.append(localSticker.instance())
            }
            
        }.main{
            block()
        }
    }
    
    func setupButton(_ button: UIButton) {
        let spacing: CGFloat = 6.0
        let imageSize: CGSize = button.imageView!.image!.size
        button.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0);
        let labelString = NSString(string: button.currentTitle!)
        let titleSize = labelString.size(attributes: [NSFontAttributeName: button.titleLabel!.font])
        button.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width);
    }
    
    //MARK:- Actions
    /* IBAction */
    
    func btnFilterAction() {
        if currentMenu == .filter {
            return
        }
        cancelLocationView()
        currentMenu = .filter
        showMenu(true)
        collectionView.reloadData()
    }
    
    func btnEffectAction() {
        if currentMenu == .effect {
            return
        }
        cancelLocationView()
        currentMenu = .effect
        showMenu(true)
        collectionView.reloadData()
    }
    
    func btnFrameAction() {
        if currentMenu == .frame {
            return
        }
        cancelLocationView()
        currentMenu = .frame
        showMenu(true)
        collectionView.reloadData()
    }
    
    func btnTextAction() {
        if currentMenu == .text {
            return
        }
        cancelLocationView()
        currentMenu = .text
        showMenu(true)
        collectionView.reloadData()
    }
    
    func btnStickerAction() {
        if currentMenu == .stickerCategory {
            return
        }
        cancelLocationView()
        currentMenu = .stickerCategory
        showMenu(true)
        if stickerLists.count == 0 {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            loadLocalSticker { [weak self] in
                self?.collectionView.reloadData()
                MBProgressHUD.hide(for: self?.view, animated: true)
            }
            return
        }
        self.collectionView.reloadData()
    }
    
    fileprivate func btnLocationAction() {
        
        if currentMenu == .location {
            return
        }
        
        collectionView.reloadData()
        
        currentMenu = .location
  
        currentSlideAction = nil
        
        self.btnCropOK.setImage(UIImage(named: "ok-large"), for: UIControlState())
        self.btnCropCancel.setImage(UIImage(named: "close-large"), for: UIControlState())
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let locationManager = LocationManager.sharedInstance
        locationManager.showVerboseMessage = true
        locationManager.autoUpdate = true
        locationManager.startUpdatingLocationWithCompletionHandler {[weak self] (latitude, longitude, status, verboseMessage, error) -> () in
            
            guard let `self` = self else {
                return
            }
            
            guard let _ = self.currentMenu else {
                return
            }

            
            if self.gotLocation {
                return
            }
                        
            if latitude != 0 && longitude != 0 {
                self.gotLocation = true
                
                locationManager.stopUpdatingLocation()
                
                let location = CLLocation(latitude: latitude, longitude: longitude)

                self.location = location
                
                GetProvinceService.request(location) { (success, venues) in
                    
                    MBProgressHUD.hide(for: self.view, animated: true)

                    if !success {
                        return
                    }
                    
                    guard let venues = venues else {
                        return
                    }
                    
                    if let venue = venues.first {
                        self.generateLocationScrollView(venue)
                    }
                    
                    self.generateLocationSelectorView(venues)

                }
            }
            else if error != nil{
                UIAlertView(title: status, message: verboseMessage, delegate: nil, cancelButtonTitle: "OK").show()
                MBProgressHUD.hide(for: self.view, animated: true)
                self.defaultState()
            }
        }
    }
    
    @IBAction func shareAction(_ sender: AnyObject) {

        clearEditingStickerHandles()

        let shareVC = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
        
        self.addChildViewController(shareVC)
        shareVC.willMove(toParentViewController: self)
        shareVC.image = generateCurrentImage()

        shareVC.view.alpha = 0
        self.view.addSubview(shareVC.view)
        
        UIView.animate(withDuration: 0.25, animations: {
            shareVC.view.alpha = 1
        }) 
    }
    
    func generateCurrentImage() -> UIImage{
        let rect = AVMakeRect(aspectRatio: self.image!.size, insideRect: cropView.bounds)
        var rect2 = cropView.convert(rect, to: nil)
        rect2 = CGRect(x: ceil(rect2.origin.x), y:floor(rect2.origin.y), width: floor(rect2.size.width), height: floor(rect2.size.height))

        UIGraphicsBeginImageContextWithOptions(rect2.size, true, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: -rect2.origin.x, y: -rect2.origin.y)
        self.view.layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    
    @IBAction func nextAction(_ sender: AnyObject) {
        
        let stickerVC = self.storyboard?.instantiateViewController(withIdentifier: "StickerViewController") as! StickerViewController
        stickerVC.image = self.cropView.image
        self.navigationController?.pushViewController(stickerVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuExpandAction(_ slider: AnyObject) {
        showMenu(toggle : true)
    }
    
    func intensityAction(_ name : String = "Intensity") {
        lblAdjustTitle.text = name
        
        currentSlideAction = .slideFilterIntensity

        adjustProgressBar.slideAction = currentSlideAction
        
        showAdjustCollectionControl(name, show: true, mode: .progress)
        
        adjustProgressBar.value = Float(intensity * 100)

    }
    
    func sharpenAction(_ name : String = "Sharpen") {
        lblAdjustTitle.text = name
        currentSlideAction = .slideShapen

        adjustProgressBar.slideAction = currentSlideAction

        showAdjustCollectionControl(name, show: true, mode: .progress)
        if let currentSlideAction = currentSlideAction, let progress = progressBarCurrent[currentSlideAction] {
            adjustProgressBar.value = Float(progress)
        } else{
            adjustProgressBar.value = 0
        }
    }
    
    func whiteAction(_ name : String = "Fade") {
        lblAdjustTitle.text = name
        currentSlideAction = .slideWhite
        
        adjustProgressBar.slideAction = currentSlideAction
        
        showAdjustCollectionControl(name, show: true, mode: .progress)
        if let currentSlideAction = currentSlideAction, let progress = progressBarCurrent[currentSlideAction] {
            adjustProgressBar.value = Float(progress)
        } else{
            adjustProgressBar.value = 100
        }
    }
    
    func contrastAction() {
        currentSlideAction = .slideContrast
        
        adjustProgressBar.slideAction = currentSlideAction
        
        showAdjustCollectionControl("Contrast", show: true, mode: .progress)
        if let currentSlideAction = currentSlideAction, let progress = progressBarCurrent[currentSlideAction] {
            adjustProgressBar.value = Float(progress)
        } else{
            adjustProgressBar.value = 0
        }
    }
    
    func brightnessAction() {
        currentSlideAction = .slideBrightness
        
        adjustProgressBar.slideAction = currentSlideAction
        
        showAdjustCollectionControl("Brightness", show: true, mode: .progress)
        
        if let currentSlideAction = currentSlideAction, let progress = progressBarCurrent[currentSlideAction] {
            adjustProgressBar.value = Float(progress)
        } else{
            adjustProgressBar.value = 0
        }
    }
    
    func saturationAction() {
        currentSlideAction = .slideSaturation

        adjustProgressBar.slideAction = currentSlideAction

        showAdjustCollectionControl("Saturation", show: true, mode: .progress)
        
        if let currentSlideAction = currentSlideAction, let progress = progressBarCurrent[currentSlideAction] {
            adjustProgressBar.value = Float(progress)
        } else{
            adjustProgressBar.value = 0
        }
    }
    
    func fontAction(){
        self.currentAdjustMenu = .adjustFont
        self.showAdjustCollectionControl("Font", show: true, mode: .collection)
        self.commandView?.removeFromSuperview()
    }
    
    func fontColorAction(){
        
        if self.currentEditingTextSticker == nil {
            return
        }
        
        self.commandView?.removeFromSuperview()
        
        self.currentAdjustMenu = .adjustFontColor
        self.showAdjustCollectionControl("Color", show: true, mode: .collection)
    }
    
    func fontFadeAction(){
        
        if currentEditingTextSticker == nil {
            return
        }
        
        self.commandView?.removeFromSuperview()

        currentSlideAction = .slideAdjustFontOpacity
        
        adjustProgressBar.slideAction = currentSlideAction

        showAdjustCollectionControl("Opacity", show: true, mode: .progress)

        if let currentEditingTextSticker = currentEditingTextSticker, currentEditingTextSticker.isText{
            let oldOpacity = currentEditingTextSticker.userData["opacity"] as! CGFloat
            let progressValue = ((Float(oldOpacity) - 0.2) / 0.8) * 100
            adjustProgressBar.value = progressValue
        } else {
            adjustProgressBar.value = 100
        }
        
    }
    
    func cropAction(){
        
        showMenu(false)
        showAdjustCollectionControl("CROP", show: true, mode: .okCancel)

        hiddenStickerViewAndFrame(true)
        hideFilterAndEffect(true)
        hideLocation(true)
        
        cropView.showOverlayViewAnimated(true, withCropFrame: nil, completion: { [weak self] () -> Void in
            print("Frame active")
            
            guard let strongSelf = self else { return }
            
            strongSelf.currentSlideAction = .crop
        })
    }
    
    
    func rotateAction() {
        showMenu(false)
        showAdjustCollectionControl("ROTATE", show: true, mode : .rotate)
        
        hiddenStickerViewAndFrame(true)
        self.hideLocation(true)
        
        self.currentSlideAction = .rotate
    }
    
    func hiddenStickerViewAndFrame(_ hide : Bool) {
        
        self.cropView!.subviews.forEach { view in
            if let view = view as? NTStickerView {
                view.hideBorder()
                view.isHidden = hide
            }
        }
        
        if hide {
            frame?.isHidden = true
        } else {
            if let row = frame?.tag {
                frame?.removeFromSuperview()
                frame = nil
                addFrameAction(row)
            }
        }
    }
    
    func hideFilterAndEffect(_ hide : Bool) {
    
        if hide {
            filterImageEffectWithParameter(intensity : 0)
        } else {
            configureImage()
        }
    }
    
    func hideLocation(_ hide : Bool) {
        
        guard let viewLocation = self.viewLocation else {
            return
        }

        if hide {
            viewLocation.alpha = 0
        } else {
            self.setupLocationViewFrameToCropView(viewLocation: viewLocation)
            viewLocation.alpha = 1
        }
    }
    
    func addFrameAction(_ row : Int){
        
        AnswerHelper.logSelectFrame(frameTemplates[row].name)

        //if frameItem.image == nil && frameItem.color == nil{
        if row == 0 {
            frame?.removeFromSuperview()
            frame = nil
            return
        }
        else {
            
            let frameItem = frameTemplates[row]
            
            var rect = AVMakeRect(aspectRatio: self.image!.size, insideRect: self.cropView.bounds)

            rect.origin.x = floor(rect.origin.x)
            rect.origin.y = floor(rect.origin.y)
            rect.size.width = ceil(rect.size.width)
            rect.size.height = ceil(rect.size.height)
            
            UIGraphicsBeginImageContextWithOptions(rect.size, false, UIScreen.main.scale)
            let context = UIGraphicsGetCurrentContext()
            
            if !frameItem.color.isEmpty {
                context?.setFillColor(frameItem.getUIColor.cgColor);
                
                let rect = CGRect(origin: CGPoint.zero, size: rect.size)
                if row != 4 {
                    context?.fill(rect)
                }
            }
            else if frameItem.image != nil {
                let s = frameItem.image.size
                context?.draw(frameItem.image!.cgImage!, in: CGRect(origin: CGPoint.zero, size: CGSize(width: s.width, height: s.height)), byTiling : true)
            }
            
            var holeRect = CGRect(origin: CGPoint.zero, size: rect.size)
            var cornerRadius : CGFloat = 10
            
            if row == 3 {
                context?.draw(frameItem.image!.cgImage!, in: CGRect(origin: CGPoint.zero, size: CGSize(width: 40, height: 40)), byTiling : true)
                holeRect = holeRect.insetBy(dx: 12, dy: 12)
            }
            else if row == 4 {
                
                let moviePieceTop = UIImage(named: "movie2")!
                let moviePieceBottom = UIImage(named: "movie")!
 
                let numMoviePiece = 16
                
                let moviePieceWidth : CGFloat = 29 //moviePieceTop.size.width
                let moviePieceHeight : CGFloat = 40 //moviePieceTop.size.height
                
                let blackgroundRect = CGRect(origin: CGPoint(x: 0, y: moviePieceHeight), size: CGSize(width: rect.size.width, height: rect.size.height - moviePieceHeight*2))
                context?.fill(blackgroundRect)
                
                for i in 0...numMoviePiece-1 {
                    let movieFrameTopRect = CGRect(origin: CGPoint(x: moviePieceWidth * CGFloat(i), y: 0), size: CGSize(width: moviePieceWidth, height: moviePieceHeight))
                    
                    context?.draw(moviePieceTop.cgImage!, in: movieFrameTopRect)
                    
                    print(movieFrameTopRect)

                    let movieFrameBottomRect = CGRect(origin: CGPoint(x: moviePieceWidth * CGFloat(i), y: rect.height - moviePieceHeight), size: CGSize(width: moviePieceWidth, height: moviePieceHeight))
                                        
                    context?.draw(moviePieceBottom.cgImage!, in: movieFrameBottomRect)
                }
                

                holeRect = CGRect(origin: CGPoint(x: 6, y: moviePieceHeight), size: CGSize(width: rect.size.width-12, height: rect.size.height-moviePieceHeight*2))

                cornerRadius = 4
                
            }
            else if row == 5 {
                holeRect = CGRect(origin: CGPoint(x: 10, y: 10), size: CGSize(width: rect.size.width-20, height: rect.size.height/1.5))
                holeRect = holeRect.insetBy(dx: 6, dy: 6)

                cornerRadius = 2
            }
            else {
                holeRect = holeRect.insetBy(dx: 6, dy: 6)
            }
        
            let clippath = UIBezierPath(roundedRect: holeRect , cornerRadius: cornerRadius)
            
            context?.addPath(clippath.cgPath)
            context?.clip()
            context?.clear(holeRect)
            
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext();
            
            if frame == nil {
                frame = UIImageView(image:newImage)
                frame!.tag = row
                frame!.frame = rect
                self.cropView.addSubview(frame!)
            }
            else{
                frame!.tag = row
                frame!.image = newImage
            }
 
        }
    }
    
    //MARK:- OK/Cancel
    
    @IBAction func adjustCancelAction(_ sender: AnyObject) {
        
        if let currentSlideAction = currentSlideAction {
            
            if currentSlideAction == .rotate {
                
                UIView.animate(withDuration: 0.3, animations: {[weak self] in
                    guard let s = self else { return }
                    s.cropView.transform = CGAffineTransform(rotationAngle: 0 * CGFloat(Double.pi/180))
                },completion : { [weak self] success in
                    guard let s = self else { return }
                    let revertAngle = -s.imageRotateDegree.tempDegree
                    let rotatedImage = s.cropView.image.imageRotatedByDegrees(revertAngle, flip: false)
                    s.cropView.image = rotatedImage
                })
                
                self.imageRotateDegree.tempDegree = 0

                showMenu(true)
                self.collectionView.reloadData()

                self.hiddenStickerViewAndFrame(false)
                self.hideLocation(false)
            }
            
            switch currentSlideAction {
            case .slideFilterIntensity:
                intensity = filterValueCurrent[currentSlideAction]!
                configureImage()
            case .slideBrightness :
                brightnessLevel = filterValueCurrent[currentSlideAction]!
                configureImage()
            case .slideContrast :
                contrastLevel = filterValueCurrent[currentSlideAction]!
                configureImage()
            case .slideSaturation :
                saturationLevel = filterValueCurrent[currentSlideAction]!
                configureImage()
            case .slideShapen :
                sharpenLevel = filterValueCurrent[currentSlideAction]!
                configureImage()
            case .slideWhite :
                whiteLevel = filterValueCurrent[currentSlideAction]!
                configureImage()
            default:break
            }
        }
        
        if let currentMenu = currentMenu {
            if currentMenu == .location {
                self.defaultState()
            }
            else if currentMenu == .filter || currentMenu == .effect || currentMenu == .text{
                showMenu(true)
                self.collectionView.reloadData()
            }
            else{
                showMenu(true)
                showAdjustCollectionControl("", show: false, mode: .progress)
            }
        }
        
        if let currentAdjustMenu = currentAdjustMenu {
            
            if currentAdjustMenu == .adjustFontColor || currentAdjustMenu == .adjustFont {
                
                if let new = currentEditingTextSticker?.userData["new"] as? Bool, new == true {
                    self.deleteStickerText()
                }
                else {
                    if let text = currentEditingTextSticker?.userData["text"] as? String {
                        let font = currentEditingTextSticker?.userData["font"] as! UIFont
                        let color = currentEditingTextSticker?.userData["fontcolor"] as! UIColor
                        addText(font, textColor: color, text: text, replace: true)
                    }
                }
            }
        }
        
        if let currentSlideAction = currentSlideAction, currentSlideAction == .slideAdjustFontOpacity  {
            if let currentEditingTextSticker = currentEditingTextSticker, currentEditingTextSticker.isText{
                let oldOpacity = currentEditingTextSticker.userData["opacity"] as! CGFloat
                opacityText(oldOpacity)
            }
        }
        
        if self.cropView.overlayViewIsActive {
            cropView.dismissOverlayViewAnimated(true, completion: { [weak self] in
                self?.hiddenStickerViewAndFrame(false)
                self?.hideFilterAndEffect(false)
            })
        }
        
        currentAdjustMenu = nil
        currentEditingTextSticker?.hideBorder()
        currentEditingTextSticker = nil
        currentSlideAction = nil
    }
    
    func cancelLocationView(){
        if self.scrollviewLocation != nil {
            self.pageControlLocation?.removeFromSuperview()
            self.pageControlLocation = nil
            self.scrollviewLocation.removeFromSuperview()
            self.scrollviewLocation = nil
            self.gotLocation = false
            self.locationScrollPage = 0
            
            if let locationSelector = self.view.viewWithTag(LOCATION_SELECTOR_TAG) {
                locationSelector.removeFromSuperview()
            }
        }
    }
    
    func generateLocationSelectorView(_ venues : [JSONParameters]) {
        
        let testVenues = [venues.first!, venues.last!]
        
        if let locationSelectorView = Bundle.main.loadNibNamed("LocationSelector", owner: self, options: nil)?.first as? LocationSelector {
            locationSelectorView.venues = testVenues
            locationSelectorView.delegate = self
            locationSelectorView.frame = CGRect(x: 0, y: 44, width: self.view.frame.width, height: 60)
            locationSelectorView.tag = LOCATION_SELECTOR_TAG
            self.view.addSubview(locationSelectorView)
        }
    }
    
    func generateLocationScrollView(_ venue : JSONParameters) {
        
        if self.viewLocation != nil {
            self.viewLocation?.removeFromSuperview()
            self.viewLocation = nil
        }
        
        var name = "None"
        var country = "None"
        let datetime = Date().stringFromDateFormatter("MMM dd, YYYY")
        
        if let n = venue["name"] as? String {
            name = n
        }
        if let c = venue["location"]?["city"] as? String
            ,let cc = venue["location"]?["country"] as? String {
            country = "\(c) | \(cc)"
        }
        
        self.showMenu(false)
        self.showAdjustCollectionControl("LOCATION", show: true, mode: .okCancel)
        
        let rect = AVMakeRect(aspectRatio: self.image!.size, insideRect: self.cropView.bounds)
        let a = self.cropView.convert(rect, to: nil)
        
        self.scrollviewLocation = UIScrollView(frame: a)
        self.scrollviewLocation.delegate = self
        self.scrollviewLocation.isPagingEnabled = true
        
        if let locationTemplate = Bundle.main.loadNibNamed("LocationView", owner: self, options: nil) as? [LocationView] {
            
            for i in 0 ..< locationTemplate.count {
                let template = locationTemplate[i]
                template.lblDateTime?.text = datetime
                template.lblPlaceName?.text = name
                template.lblCountry?.text = country
                
                if template.isCenter {
                    let x = (CGFloat(i) * a.width) + self.scrollviewLocation.bounds.midX
                    let y = self.scrollviewLocation.bounds.midY
                    template.center = CGPoint(x: x, y: y)
                }
                else if template.isStrechHorizontal {
                    template.frame = CGRect(x: CGFloat(i) * a.width, y: self.scrollviewLocation.frame.height - template.frame.height, width: self.scrollviewLocation.frame.width, height: template.frame.height)
                }
                else {
                    template.frame = CGRect(x: CGFloat(i) * a.width + self.scrollviewLocation.frame.width - template.frame.size.width, y: self.scrollviewLocation.frame.height - template.frame.height, width: template.frame.width, height: template.frame.height)
                }
                
                
                self.scrollviewLocation.addSubview(template)
            }
            
            self.scrollviewLocation.contentSize = CGSize(width: a.width * CGFloat(locationTemplate.count) , height: a.height)
            
            self.pageControlLocation = UIPageControl(frame: CGRect(x: a.origin.x,y: a.origin.y + a.size.height,width: a.size.width-2,height: 20))
            self.pageControlLocation!.numberOfPages = locationTemplate.count
            self.pageControlLocation!.isEnabled = false
            self.scrollviewLocation.setContentOffset(CGPoint(x: CGFloat(self.locationScrollPage) * a.width, y:0), animated: false)
            
            self.view.addSubview(self.scrollviewLocation)
            self.view.addSubview(self.pageControlLocation!)
            
        }
        
    }
    
    func applyLocationScrollView() {
        if scrollviewLocation != nil {
            
            guard let viewLocation = scrollviewLocation.subviews[self.locationScrollPage] as? LocationView else { return }
            
            AnswerHelper.logSelectLocationTemplate(self.locationScrollPage+1)
            
            self.viewLocation = viewLocation

            setupLocationViewFrameToCropView(viewLocation: viewLocation)
            
            self.cropView.addSubview(viewLocation)
            
            scrollviewLocation.removeFromSuperview()
            scrollviewLocation = nil
            pageControlLocation?.removeFromSuperview()
            pageControlLocation = nil
            
            if let locationSelector = self.view.viewWithTag(LOCATION_SELECTOR_TAG) {
                locationSelector.removeFromSuperview()
            }
            
            self.gotLocation = false
            
            defaultState()
        }
    }
    
    fileprivate func setupLocationViewFrameToCropView(viewLocation : LocationView) {
        var rect = AVMakeRect(aspectRatio: self.image!.size, insideRect: self.cropView.bounds)
        rect.origin.x = floor(rect.origin.x)
        rect.origin.y = floor(rect.origin.y)
        rect.size.width = ceil(rect.size.width)
        rect.size.height = ceil(rect.size.height)
        
        if viewLocation.isCenter {
            let x = rect.midX
            let y = rect.midY
            viewLocation.center = CGPoint(x: x, y: y)
        }
        else if viewLocation.isStrechHorizontal  {
            let newFrame = CGRect(origin: CGPoint(x: rect.origin.x, y: rect.origin.y + rect.size.height - viewLocation.frame.size.height), size: CGSize(width: rect.size.width, height: viewLocation.frame.size.height))
            viewLocation.frame = newFrame
        }
        else {
            let newFrame = CGRect(origin: CGPoint(x: rect.size.width - viewLocation.frame.size.width, y: rect.origin.y + rect.size.height - viewLocation.frame.size.height), size: viewLocation.frame.size)
            viewLocation.frame = newFrame
        }
        
    }
    
    @IBAction func adjustOKAction(_ sender: AnyObject) {

        if let currentSlideAction = currentSlideAction {

            if currentSlideAction == .rotate {
                self.cropView.transform = CGAffineTransform(rotationAngle: 0 * CGFloat(Double.pi/180))
                let rotatedImage = self.cropView.image.imageRotatedByDegrees(self.imageRotateDegree.tempDegree, flip: false)
                self.cropView.image = rotatedImage
                self.image = self.image!.imageRotatedByDegrees(self.imageRotateDegree.tempDegree, flip: false)
                self.imageRotateDegree.finalDegree = self.imageRotateDegree.finalDegree + self.imageRotateDegree.tempDegree
                self.imageRotateDegree.tempDegree = 0
                self.editingStatus.isRotated = self.imageRotateDegree.finalDegree != 0
                
                self.hiddenStickerViewAndFrame(false)
                self.hideLocation(false)
            }
  
            progressBarCurrent[currentSlideAction] = self.valueProgress
            filterValueCurrent[currentSlideAction] = self.realValueProgress
        }
        
        if let currentMenu = currentMenu {
            if currentMenu == .location {
                self.defaultState()
            }
            else if currentMenu == .filter || currentMenu == .effect || currentMenu == .text{
                showMenu(true)
                self.collectionView.reloadData()
            }
        }
        
        
        if let currentAdjustMenu = currentAdjustMenu, currentAdjustMenu == .adjustFont {
            currentEditingTextSticker?.userData["new"] = false
            if let newFont = currentEditingTextSticker?.userData["newFont"] {
                currentEditingTextSticker?.userData["font"] = newFont
            }
        }
        
        if let currentAdjustMenu = currentAdjustMenu, currentAdjustMenu == .adjustFontColor {
            if let newFontColor = currentEditingTextSticker?.userData["newFontcolor"] {
                currentEditingTextSticker?.userData["fontcolor"] = newFontColor
            }
        }
        
        
        if let currentSlideAction = currentSlideAction, currentSlideAction == .slideAdjustFontOpacity {
            if let opacity = currentEditingTextSticker?.userData["newOpacity"] {
                currentEditingTextSticker?.userData["opacity"] = opacity
            }
        }
        
        if self.currentMenu == .text {
            self.clearEditingStickerHandles()
        }
        
        currentAdjustMenu = nil
        currentEditingTextSticker?.hideBorder()
        currentEditingTextSticker = nil
        currentSlideAction = nil
    }
    
    @IBAction func cropOKAction(_ sender: AnyObject) {
        
        if currentMenu != nil && currentMenu == .location {
            self.applyLocationScrollView()
        }
        else if currentSlideAction == nil || currentSlideAction == .crop {
            
            if currentSlideAction == .crop {
                self.image = self.cropView.croppedImage()
                self.cropView.image = self.image
                self.cropView.dismissOverlayViewAnimated(true, completion: { [weak self] in
                    guard let weakSelf = self else { return }
                    weakSelf.hiddenStickerViewAndFrame(false)
                    weakSelf.hideFilterAndEffect(false)
                    weakSelf.hideLocation(false)
                })
                
                self.editingStatus.isCropped = true
                self.showMenu(true)
                self.collectionView.reloadData()
            }

        }
        else if currentSlideAction == .rotate {
            rotateImage(left:false)
        }
    }
    
    @IBAction func cropCancelAction(_ sender: AnyObject) {
        
        if currentMenu != nil && currentMenu == .location {
            cancelLocationView()
            defaultState()
        }
        else if currentSlideAction == nil || currentSlideAction == .crop {
            
            showMenu(true)
            self.collectionView.reloadData()

            self.currentSlideAction = nil
            self.currentAdjustMenu = nil
            
            cropView.dismissOverlayViewAnimated(true, completion: { [weak self] in
                guard let weakSelf = self else { return }
                weakSelf.hiddenStickerViewAndFrame(false)
                weakSelf.hideFilterAndEffect(false)
                weakSelf.hideLocation(false)
            })
        }
        else if currentSlideAction == .rotate{
            rotateImage(left:true)
        }
    }
    
    @IBAction func sliderTouchUp(_ slider: UISlider) {
        sliderChange(slider.value)
    }
    
    @IBAction func sliderTouchUpOutSide(_ slider: UISlider) {
        sliderChange(slider.value)
    }
    
    func rotateImage(left : Bool) {
        
        let degrees:CGFloat = left ? -90 : 90
        
        self.imageRotateDegree.tempDegree = self.imageRotateDegree.tempDegree + degrees
        self.imageRotateDegree.tempDegree = (self.imageRotateDegree.tempDegree).truncatingRemainder(dividingBy: 360)
        
        UIView.animate(withDuration: 0.15, animations: {[weak self] in
            guard let s = self else { return }
            s.cropView.transform = CGAffineTransform(rotationAngle: s.imageRotateDegree.tempDegree * CGFloat(M_PI/180))
        },completion : { success in })
    }
    
    func configureImage(){
        filterImageEffectWithParameter(intensity : self.intensity, contrastLevel: contrastLevel, sharpenLevel: sharpenLevel, brightnessLevel: brightnessLevel, saturationLevel: saturationLevel, whiteLevel: whiteLevel)
    }
    
    func filterImageEffectWithParameter(intensity : CGFloat = 1, contrastLevel : CGFloat = 1, sharpenLevel : CGFloat = 0, brightnessLevel : CGFloat = 0, saturationLevel : CGFloat = 1, whiteLevel : CGFloat = 1) {
        
        FilterHelper.configImage(currentFilter, intensity: intensity, contrastLevel : contrastLevel, sharpenLevel: sharpenLevel, brightnessLevel: brightnessLevel, saturationLevel: saturationLevel, originalImage: self.image!, callback: { [weak self] (filteredImage) in
            guard let weakSelf = self else { return }
            //Rotate to specific degree
            weakSelf.cropView.image = filteredImage
            weakSelf.cropView.alpha = whiteLevel
        })
    }
    
    func defaultEffectLevel() {
        brightnessLevel = 0
        contrastLevel = 1
        saturationLevel = 1
        sharpenLevel = 0
        whiteLevel = 1
    }

    
    func sliderChange(_ value : Float){
        
        if currentSlideAction == .slideContrast {
            contrastLevel = CGFloat(value)
            configureImage()
        }
        else if currentSlideAction == .slideBrightness {
            brightnessLevel = CGFloat(value)
            configureImage()
        }
        else if currentSlideAction == .slideSaturation {
            saturationLevel = CGFloat(value)
            configureImage()
        }
        else if currentSlideAction == .slideShapen {
            sharpenLevel = CGFloat(value)
            configureImage()
        }
        else if currentSlideAction == .slideWhite {
            whiteLevel = CGFloat(value)
            configureImage()
        }
        else if currentSlideAction == .slideFilterIntensity {
            intensity = CGFloat(value)
            filterImage(currentFilter!)
        }
        else if currentSlideAction == .slideAdjustFontOpacity {
            let opacityValue = CGFloat(value)
            if let currentEditingTextSticker = currentEditingTextSticker, currentEditingTextSticker.isText{
                currentEditingTextSticker.userData["newOpacity"] = opacityValue
            }
            opacityText(opacityValue)
        }
    }
    
    
    func showMenu(_ show : Bool = true, toggle : Bool = false){
        
        self.showAdjustCollectionControl("", show: false, mode: .progress)
        
        UIView.animate(withDuration: 0.3, animations: {
            self.filterBar.alpha = show ? 1 : 0
        }) 
    }
    
    
    func showAdjustCollectionControl(_ text : String ,show : Bool = true, mode : AdjustCollectionMode){
        
        guard show else {
            UIView.animate(withDuration: 0.3, animations: {
                self.adjustBar.alpha = 0
            }) 
            return
        }
        
        self.btnOKAdjust.isHidden = false
        self.btnCancelAdjust.isHidden = false
        
        if mode == .okCancel || mode == .rotate{
            self.adjustProgressBar.isHidden = true
            self.collectionAdjust.isHidden = true
            self.correctWrongView.isHidden = false
            
            if mode == .rotate {
                self.btnCropOK.setImage(UIImage(named: "rotate-right"), for: UIControlState())
                self.btnCropCancel.setImage(UIImage(named: "rotate-left"), for: UIControlState())
            }
            else if mode == .okCancel{
                self.btnOKAdjust.isHidden = true
                self.btnCancelAdjust.isHidden = true
                self.btnCropOK.setImage(UIImage(named: "ok-small"), for: UIControlState())
                self.btnCropCancel.setImage(UIImage(named: "close-small"), for: UIControlState())
            }
            
        }
        else if mode == .progress {
            self.adjustProgressBar.isHidden = false
            self.collectionAdjust.isHidden = true
            self.correctWrongView.isHidden = true
        }
        else if mode == .collection {
            self.collectionAdjust.isHidden = false
            self.collectionAdjust.reloadData()
            self.adjustProgressBar.isHidden = true
            self.correctWrongView.isHidden = true
        }
        

        self.lblAdjustTitle.text = text.uppercased()

        UIView.animate(withDuration: 0.3, animations: {
            self.adjustBar.alpha = show ? 1 : 0
        }) 
    }
    
    func addText(_ f : UIFont?, textColor : UIColor = UIColor.white, text : String = "DOUBLE TAP TO EDIT", replace : Bool = false) {
        
        let label = UILabel()
        
        var font = UIFont.systemFont(ofSize: 24)
        
        if let f = f {
            font = f
        }
        
        var textAlignment : NSTextAlignment = .left
        if replace && currentEditingTextSticker != nil{
            textAlignment = NSTextAlignment(rawValue: currentEditingTextSticker?.userData["alignment"] as! Int)!
        }
        
        label.font = font
        label.text = text
        label.textColor = textColor
        label.numberOfLines = 0
        label.textAlignment = textAlignment
        label.lineBreakMode = .byWordWrapping
        
        let imageRect = AVMakeRect(aspectRatio: self.image!.size, insideRect: self.cropView.bounds)

        let size = CGSize(width: imageRect.size.width, height: CGFloat.greatestFiniteMagnitude)
    
        var rect = (label.text! as NSString).boundingRect(with: size, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSFontAttributeName :font], context: nil)
        
        //captureView view is needed for capture.
        let captureView = UIView(frame: rect)
        captureView.backgroundColor = UIColor.clear
        label.frame = captureView.bounds
        captureView.addSubview(label)
        
        UIGraphicsBeginImageContextWithOptions(rect.size, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()!
        context.clear(rect)
        label.layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let textImage = UIImageView(image: image)
        textImage.contentMode = .scaleAspectFit
        
        rect = CGRect(origin: CGPoint.zero, size: CGSize(width: rect.size.width, height: rect.height + NTStickerView.BUTTONSIZE))

        if replace {
            
            guard let currentEditingTextSticker = currentEditingTextSticker else {
                return
            }
            let oldCenter = currentEditingTextSticker.center
//            currentEditingTextSticker.bounds = CGRect(origin: currentEditingTextSticker.frame.origin, size: rect.size)
            //currentEditingTextSticker.frame.size = rect.size
            currentEditingTextSticker.center = oldCenter
            currentEditingTextSticker.contentView = textImage
            currentEditingTextSticker.userData["newFontcolor"] = textColor
            
            if currentEditingTextSticker.userData["new"] as! Bool == true {
                currentEditingTextSticker.userData["font"] = font
            }
            else {
                if let _ = currentEditingTextSticker.userData["font"] as? UIFont {
                    currentEditingTextSticker.userData["newFont"] = font
                }
            }
            
            if let opacity = currentEditingTextSticker.userData["opacity"] as? CGFloat {
                opacityText(opacity)
            }
            
            return
        }
        
        //due to NTStickerView has padding for contentView so we need to add more height
        let labelView = NTStickerView(frame : rect)
        labelView.center = CGPoint(x: imageRect.midX, y: imageRect.midY)
        labelView.isText = true
        labelView.userData = [ "font" as NSObject : font
            ,"text" as NSObject : label.text! as AnyObject
            ,"alignment" as NSObject : label.textAlignment.rawValue as AnyObject
            ,"fontcolor" as NSObject : textColor
            ,"opacity" as NSObject  : CGFloat(1)
            ,"new" : true
        ]

        labelView.contentView = textImage
        labelView.delegate = self
        self.cropView.addSubview(labelView)
        currentEditingTextSticker = labelView
        //clearEditingStickerHandles()
        
        
    }
    
//    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
//        super.touchesBegan(touches, withEvent: event)
//        clearEditingStickerHandles()
//    }
    
    func cropViewTapGestureAction(_ gesture : UITapGestureRecognizer){
//        let location = gesture.location(in: gesture.view!)
//        let subview = gesture.view!.hitTest(location, with: nil)
//        
//        if !(subview is NTStickerView) {
//            clearEditingStickerHandles()
//        }
    }
    
    func clearEditingStickerHandles(){
        
        if currentAdjustMenu != nil {
            return
        }
        
        self.cropView!.subviews.forEach { view in
            if let view = view as? NTStickerView {
                view.hideBorder()
                currentEditingTextSticker = nil
            }
        }
    }

}


extension FilterViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        if collectionView == collectionViewMenu {
            return menuTemplates.count
        }
        
        if collectionView == collectionAdjust {
            if currentAdjustMenu == .adjustFont {
                return fontsList.count
            }
            else if currentAdjustMenu == .adjustFontColor {
                return colorList.count
            }
        }
        
        if currentMenu == .filter {
            return filterTemplates.count
        }
        else if currentMenu == .effect {
            return effectTemplates.count
        }
        else if currentMenu == .text {
            return textTemplates.count
        }
        else if currentMenu == .stickerCategory {
            return stickerLists.count + 1
        }
        else if currentMenu == .sticker {
            return stickerLists[currentCategorySelectIndex].stickerFilesWithPath().count + 1
        }
        else if currentMenu == .frame {
            return frameTemplates.count
        }

        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        if collectionView == collectionViewMenu {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: indexPath) as! MenuCollectionViewCell
    
            let menu = menuTemplates[indexPath.row]
            cell.setCell(menu)
            return cell
        }
        
        if collectionView == collectionAdjust {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdjustCell", for: indexPath) as! AdjustCollectionCell
                        
            if currentAdjustMenu == .adjustFont {
                cell.lblTextFont.text = fontsList[indexPath.item].example
                cell.lblTextFont.font = fontsList[indexPath.item].font
            }
            else if currentAdjustMenu == .adjustFontColor {
                let row = indexPath.item
                cell.textColorView.layer.cornerRadius = cell.textColorView.frame.height / 2
                cell.textColorView.backgroundColor = colorList[row]
                if row == 0 {
                    cell.textColorView.layer.borderColor = UIColor.white.cgColor
                    cell.textColorView.layer.borderWidth = 1
                }
            }
            
            return cell
        }
        
        let row = indexPath.item

        if currentMenu == .sticker {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "filterImageCell", for: indexPath) as! FilterStickerCell

            if row == 0 {
                cell.thumbnail.contentMode = .center
                cell.thumbnail.image = UIImage(named:"close-small")
            }else{
                let imagePath = stickerLists[currentCategorySelectIndex].stickerFilesWithPath()[row - 1]
                cell.thumbnail.image = nil
                var decryptData: Data?
                Async.background {
                    
                    if let data = try? Data(contentsOf: URL(fileURLWithPath: imagePath)) {
                        let password = KeychainWrapper.standard.string(forKey:KeychainKey)
                        do  {
                            decryptData = try RNCryptor.decrypt(data: data, withPassword: password!)
                        }catch { }
                    }
                }.main {
                    if let decryptData = decryptData {
                        cell.thumbnail.contentMode = .scaleAspectFit
                        cell.thumbnail.image = UIImage(data: decryptData)
                    }
                }
            }
            
            return cell
        }
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "filterCell", for: indexPath) as? FilterCollectionCell else {
            return UICollectionViewCell()
        }
        
        if currentMenu == .filter {
            
            let template = filterTemplates[indexPath.item]
            
            cell.thumbnail.contentMode = .scaleAspectFit
            cell.lblFilterName?.text = template.name
            cell.thumbnail.image = self.tempFilter?[indexPath.row]
            
        }
        else if currentMenu == .effect {
            cell.thumbnail.contentMode = .scaleAspectFit
            let item = effectTemplates[indexPath.item]
            cell.setCellWithActiveStatus(item, editingStatus: self.editingStatus, progressValue: self.progressBarCurrent)
        }
        else if currentMenu == .text {
            
            let item = textTemplates[indexPath.item]
            cell.setCell(item)

            if indexPath.item == 1 || indexPath.item == 2 {
                cell.enableButton(currentEditingTextSticker != nil)
            }
            
        }
        else if currentMenu == .frame {

            let item = frameTemplates[indexPath.item]
            cell.setCell(item)
        }
        else if currentMenu == .stickerCategory {

            if row == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shopingCartCell", for: indexPath)
                return cell
            }
            else{
                let row = indexPath.item
                let localSticker = stickerLists[row - 1]
                cell.lblFilterName?.text = localSticker.stickerName
                cell.thumbnail.image = nil
                let imagePath = localSticker.stickerFilesWithPath()[localSticker.stickerThumbnailIndex]
                var decryptData : Data?
                Async.background{
                    if let data = try? Data(contentsOf: URL(fileURLWithPath: imagePath)) {
                        let password = KeychainWrapper.standard.string(forKey: KeychainKey)
                        do  {
                            decryptData = try RNCryptor.decrypt(data: data, withPassword: password!)
                        }catch { }
                    }
                }.main {
                    if let decryptData = decryptData {
                        cell.thumbnail.contentMode = .scaleAspectFit
                        cell.thumbnail.image = UIImage(data:  decryptData)
                    }
                }
            }
        }


        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        if collectionView == self.collectionView && currentMenu == .text && currentEditingTextSticker == nil{
            if indexPath.item == 1 || indexPath.item == 2 {
                
                return false
            }
        }
        
        return true
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewMenu {
            
            if currentSlideAction != nil {
                if currentSlideAction == .crop {
                    cropView.dismissOverlayViewAnimated(true, completion: nil)
                    currentSlideAction = nil
                }
                else{
                    adjustCancelAction("" as AnyObject)
                }
            }
            
            if currentAdjustMenu != nil {
                adjustCancelAction("" as AnyObject)
            }
            
            if indexPath.item == 0 {
                btnFilterAction()
            }
            else if indexPath.item == 1 {
                btnEffectAction()
            }
            else if indexPath.item == 2 {
                btnStickerAction()
            }
            else if indexPath.item == 3 {
                btnTextAction()
            }
            else if indexPath.item == 4 {
                btnLocationAction()
            }
            else if indexPath.item == 5 {
                btnFrameAction()
            }
            
            //When there is active sticker/text,
            //and user tap menu , so we treat that likes 'ok' to add sticker/tet
            if let _ = self.currentEditingTextSticker {
                self.okToAddSticker()
            }
            
            return
        }
        
        else if collectionView == self.collectionView {

            if currentMenu == .filter {
                
                let template = filterTemplates[indexPath.item]
                
                AnswerHelper.logSelectFilter(template.name)

                if currentFilter == template.filterTemplate.lookupFilter {
                    if indexPath.item != 0 {
                        intensityAction(template.name)
                    }
                }else{
                    let filter = template.filterTemplate.lookupFilter
                    currentFilter = filter
                    intensity = 0.5
                    filterImage(filter)
                }
            }
            else if currentMenu == .effect {
                if indexPath.item == 0 {
                    cropAction()
                }
                else if indexPath.item == 1 {
                    rotateAction()
                }
                else if indexPath.item == 2 {
                    brightnessAction()
                }
                else if indexPath.item == 3 {
                    contrastAction()
                }
                else if indexPath.item == 4 {
                    saturationAction()
                }
                else if indexPath.item == 5 {
                    sharpenAction()
                }
                else if indexPath.item == 6 {
                    whiteAction()
                }
            }
            else if currentMenu == .text {
                if indexPath.item == 0 {
                    fontAction()
                }
                else if indexPath.item == 1 {
                    fontColorAction()
                }
                else if indexPath.item == 2 {
                    fontFadeAction()
                }
            }
            else if currentMenu == .frame {
                addFrameAction(indexPath.item)
            }
            else if currentMenu == .sticker {
                if indexPath.item == 0 {
                    currentMenu = .stickerCategory
                    self.collectionView.reloadData()
                }
                else{
                    let row = indexPath.item
                    let imagePath = self.stickerLists[currentCategorySelectIndex].stickerFilesWithPath()[row - 1]
                    self.addStickerFromImagePath(imagePath)
                }
            }
            else if currentMenu == .stickerCategory {
                if indexPath.item == 0 {
                    if let shopVC = self.storyboard?.instantiateViewController(withIdentifier: "ShopViewController") as? ShopViewController {
                        
                        shopVC.delegate = self
                        
                        let nav = UINavigationController(rootViewController: shopVC)
                        nav.setNavigationBarHidden(true, animated: false)
                        self.present(nav, animated: true, completion: nil)
                    }
                }
                else {
                    let row = indexPath.item
                    currentCategorySelectIndex = row - 1
                    currentMenu = .sticker
                    self.collectionView.reloadData()

                }
            }
            
            return
        }
        
        else if collectionView == collectionAdjust {
            
            if let sticker = self.currentEditingTextSticker, !sticker.isText{
                sticker.hideBorder()
                self.currentEditingTextSticker = nil
            }
         
            if currentAdjustMenu == .adjustFont {
                let font = fontsList[indexPath.item]
                let replace = self.currentEditingTextSticker != nil
                if replace == true {
                    if let text = currentEditingTextSticker?.userData["text"] as? String
                        , let color = currentEditingTextSticker?.userData["fontcolor"] as? UIColor {
                        addText(font.fontText, textColor: color, text: text, replace: replace)
                    }
                }
                else{
                    addText(font.fontText, replace : replace)
                }
            }
            else if currentAdjustMenu == .adjustFontColor {
                if let text = currentEditingTextSticker?.userData["text"] as? String,
                    let font = currentEditingTextSticker?.userData["font"] as? UIFont {
                    
                    let color = colorList[indexPath.item]
                    
                    addText(font, textColor: color, text: text, replace: true)
                }
            }
        }
        
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let oldSize = (collectionViewLayout as! UICollectionViewFlowLayout).itemSize
        
        var divider : CGFloat = 0
        var maxWidth : CGFloat = 0
        
        if collectionView == self.collectionViewMenu {
            divider = CGFloat(menuTemplates.count)
        }
        else if collectionView == self.collectionView {
            
            if currentMenu == .effect {
                return CGSize(width: 64, height: oldSize.height)
            }
            else if currentMenu == .text {
                divider = CGFloat(textTemplates.count)
            }
            else if currentMenu == .stickerCategory {
                if indexPath.item == 0 {
                    return CGSize(width: 90, height: oldSize.height)
                }
                
                return CGSize(width: 68, height: oldSize.height)
            }
            else if currentMenu == .sticker {
                
            }
            else if currentMenu == .frame {
                divider = CGFloat(frameTemplates.count)
                maxWidth = 54
            }
        }
        
        else if collectionView == self.collectionAdjust {
            if currentAdjustMenu == .adjustFont {
                divider = 6
                maxWidth = 68
            }
        }
        
        let c = collectionViewLayout as! UICollectionViewFlowLayout
        
        if divider > 0 {
            var screenWidth = UIScreen.main.bounds.width
            screenWidth = screenWidth - c.sectionInset.right - c.sectionInset.left // remove section inset.
            screenWidth = screenWidth - (c.minimumInteritemSpacing * (divider - 1))

            let widthFromPortionScreen = floor( screenWidth / divider)
            let cellWidth = max(maxWidth, widthFromPortionScreen)
            return CGSize(width: cellWidth, height: oldSize.height)
        }
        
        return oldSize
    }

    //MARK:End Collection Delegate
    
    
    func filterImage(_ filter : Filter, intensity : CGFloat = -1){
        
        if filter == .original{
            self.cropView.image = self.image!
            return
        }
        
        var finalIntensity = self.intensity
        
        if intensity > -1 {
            finalIntensity = intensity
        }
                
        FilterHelper.configImage(filter, intensity: finalIntensity, contrastLevel : contrastLevel, sharpenLevel: sharpenLevel, brightnessLevel: brightnessLevel, saturationLevel: saturationLevel, originalImage: self.image!, callback: { [weak self] (filteredImage) in
            
            guard let sSelf = self else { return }
            Async.main {
                sSelf.cropView.image = filteredImage
            }
        })
    }

    
    func addStickerFromImagePath(_ imagePath : String){
        var decryptData : Data?
        
        Async.background{
            if let data = try? Data(contentsOf: URL(fileURLWithPath: imagePath)) {
                let password = KeychainWrapper.standard.string(forKey: KeychainKey)
                do  {
                    decryptData = try RNCryptor.decrypt(data: data, withPassword: password!)
                }catch { }
            }
        }.main {
            if let decryptData = decryptData, let sticker = UIImage(data: decryptData) {
                self.addSticker(sticker)
            }
        }
        
        if let stickerName = self.stickerLists[currentCategorySelectIndex].stickerName {
            AnswerHelper.logSelectSticker(stickerName)
        }
    }
    
    func opacityText( _ value : CGFloat ){

        if let currentEditingTextSticker = currentEditingTextSticker, currentEditingTextSticker.isText{
            currentEditingTextSticker.contentView.alpha = CGFloat(value)
        }
    }

    @IBAction func deleteStickerText(_ sender: UIButton) {
        deleteStickerText()
    }
    
    func deleteStickerText(){
        currentEditingTextSticker?.hideBorder()
        currentEditingTextSticker?.removeFromSuperview()
        currentEditingTextSticker = nil
    }
}

extension FilterViewController : NTStickerViewDelegate {
    
    func isInEditTextMode() -> Bool {
        if let currentSlideAction = currentSlideAction, currentSlideAction == .slideAdjustFontOpacity {
            return true
        }
        
        if let currentAdjustMenu = currentAdjustMenu, currentAdjustMenu == .adjustFont || currentAdjustMenu == .adjustFontColor {
            return true
        }
        
        return false
    }
    
    func didSelectSticker(_ sticker: NTStickerView) {
        
        guard let currentEditingTextSticker = currentEditingTextSticker else {
            self.currentEditingTextSticker = sticker
            self.currentEditingTextSticker?.showBorder()
            return
        }
        
        if currentEditingTextSticker == sticker {
            return
        }
        
        if currentEditingTextSticker.isText && isInEditTextMode() {
            return
        }
                
        currentEditingTextSticker.hideBorder()
        self.currentEditingTextSticker = sticker
        self.currentEditingTextSticker?.showBorder()
    }
    
    func didDoubleTap(_ sticker: NTStickerView) {

        print("sticker.tag = \(sticker.tag)")
        
        //if text sticker.
        
        if sticker.isText {
            currentEditingTextSticker = sticker
            if let editTextVC = self.storyboard?.instantiateViewController(withIdentifier: "EditTextViewController") as? EditTextViewController {
                editTextVC.delegate = self
                editTextVC.userData = currentEditingTextSticker?.userData
                self.present(editTextVC, animated: true, completion: nil)
            }
        }

    }
}

extension FilterViewController : EditTextViewControllerDelegate {
    
    func didEditWithText(_ text: String, alignment: NSTextAlignment) {
        
        let label = UILabel()
        label.text = text
        label.textColor = currentEditingTextSticker?.userData["fontcolor"] as! UIColor
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.lineBreakMode = .byWordWrapping

        if let font = currentEditingTextSticker?.userData["font"] as? UIFont {
            label.font = font
        }
        
        currentEditingTextSticker?.userData["text"] = text
        currentEditingTextSticker?.userData["alignment"] = alignment.rawValue
        
        let imageRect = AVMakeRect(aspectRatio: self.image!.size, insideRect: self.cropView.bounds)

        let size = CGSize(width: imageRect.size.width, height: CGFloat.greatestFiniteMagnitude)
        
        let boundingRect = (label.text! as NSString).boundingRect(with: size, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSFontAttributeName :label.font], context: nil)
        
        let rect = CGRect(origin: CGPoint.zero, size: CGSize(width: boundingRect.size.width, height: boundingRect.height))
        
        //vv is needed, for render
        let vv = UIView(frame: rect)
        vv.backgroundColor = UIColor.clear
        label.frame = vv.bounds
        vv.addSubview(label)
        
        UIGraphicsBeginImageContextWithOptions(rect.size, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()!
        context.clear(rect)
        label.layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        
        if let currentEditingTextSticker = currentEditingTextSticker {
            let height = rect.height + NTStickerView.BUTTONSIZE
            let width = max(rect.width, height)
            currentEditingTextSticker.bounds = CGRect(origin: currentEditingTextSticker.frame.origin, size: CGSize(width: width, height: height))
            currentEditingTextSticker.contentView = imageView
            
            let oldOpacity = currentEditingTextSticker.userData["opacity"] as! CGFloat
            opacityText(oldOpacity)

        }
    }
}



var radius : CGFloat = 4
var triangleHeight : CGFloat = 6

extension FilterViewController : NTProgressBarDelegate {
    
    func onNTThumbState(_ progressView : NTProgressBar , state: ThumbState) {
        switch state {
        case .start :
            progressLabelView.isHidden = false
            break
            
        case .stop:
            sliderChange(progressView.filterValue)
            progressLabelView.isHidden = true
            break
        }
    }
    
    
    func onNTProgress(_ value: CGFloat, showingValue : CGFloat, valueProgress : CGFloat) {
        self.valueProgress = valueProgress
        self.realValueProgress = value
    }
    
    func onNTThumbMove(_ progressView : NTProgressBar, thumb: CGPoint) {
        if let label = self.progressLabelView.viewWithTag(99) as? UILabel {
            label.text = String(format: "%d", Int(progressView.value))
        }
        
        progressLabelView.center = thumb
        progressLabelView.center.y = thumb.y - 28
    }
    
    func bubblePathForContentSize(_ contentSize: CGSize) -> UIBezierPath {
        let rect = CGRect(x: 0, y: 0, width: contentSize.width, height: contentSize.height)
            //.offsetBy(dx: radius, dy: radius + triangleHeight)
        let path = UIBezierPath()
        let radius2 = radius / 2 // Radius adjasted for the border width
        
        path.move(to: CGPoint(x: rect.midX - triangleHeight, y: rect.maxY + radius2))
        path.addLine(to: CGPoint(x: rect.midX , y: rect.maxY + radius2 + triangleHeight))
        path.addLine(to: CGPoint(x: rect.midX + triangleHeight, y: rect.maxY + radius2))
        path.addArc(withCenter: CGPoint(x: rect.maxX, y: rect.maxY), radius: radius2, startAngle: CGFloat(M_PI_2), endAngle: 0, clockwise: false)
        path.addArc(withCenter: CGPoint(x: rect.maxX, y: rect.minY), radius: radius2, startAngle: 0, endAngle: CGFloat(-M_PI_2), clockwise: false)
        path.addArc(withCenter: CGPoint(x: rect.minX, y: rect.minY), radius: radius2, startAngle: CGFloat(-M_PI_2), endAngle: CGFloat(M_PI), clockwise: false)
        path.addArc(withCenter: CGPoint(x: rect.minX, y: rect.maxY), radius: radius2, startAngle: CGFloat(M_PI), endAngle: CGFloat(M_PI_2), clockwise: false)

        path.close()
        return path
    }
    
}

extension FilterViewController : ShopViewControllerFilterDelegate {

    func backFromShopAction() {
        
        self.stickerLists.removeAll()

        loadLocalSticker { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.collectionView.reloadData()
        }
    }
}

extension FilterViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = Int(round(scrollView.contentOffset.x / scrollView.frame.width))
        locationScrollPage = page
        self.pageControlLocation?.currentPage = page
    }
    
}

extension FilterViewController  {
        
    func addSticker(_ image : UIImage){
        let img = UIImageView(image: image)
        img.contentMode = .scaleAspectFit
        
        
        let rect = AVMakeRect(aspectRatio: self.image!.size, insideRect: self.cropView.bounds)
        
        let width = rect.width / 1.5
        let height = image.size.height / (image.size.width / width)
        let frame = CGRect(x: 0,y: 0, width: width, height: height)
        
        var oldCenter : CGPoint?
        if let currentSticker = currentEditingTextSticker {
            oldCenter = currentSticker.center
            currentSticker.bounds = frame
            currentSticker.center = oldCenter!
            currentSticker.contentView = img
            return
        }
        
        let stickerView = NTStickerView(frame: frame)
        stickerView.center = CGPoint(x: rect.midX, y: rect.midY)
        stickerView.delegate = self
        stickerView.contentView = img
        
        self.currentEditingTextSticker = stickerView
        self.cropView.addSubview(currentEditingTextSticker!)
    }
    
    func addStickerCommandView(){
        
        if let _ = self.commandView {
            return
        }
        
            if let commandView  = Bundle.main.loadNibNamed("StickerCommandView", owner: self, options: nil)?[0] as? UIView {
                
                //height tabbar = 44
                //height collection view = 74
                let height : CGFloat = self.currentMenu == nil ? 44.0 : 44.0 + 74.0
                commandView.frame = CGRect(x: 0, y: self.view.bounds.height - height - 44, width: self.view.bounds.width, height: 44)
                
                if let btnOK = commandView.subviews[0] as? UIButton {
                    btnOK.addTarget(self, action: #selector(okToAddSticker), for: .touchUpInside)
                }
                if let btnCancel = commandView.subviews[1] as? UIButton {
                    btnCancel.addTarget(self, action: #selector(cancelToAddSticker), for: .touchUpInside)
                }
                self.view.addSubview(commandView)
                self.commandView = commandView
            }
    

    }
    
    func okToAddSticker() {
        self.currentEditingTextSticker?.hideBorder()
        self.currentEditingTextSticker = nil
    }
    
    func cancelToAddSticker() {
        self.deleteStickerText()
    }
    
    func removeStickerCommandView() {
        self.commandView?.removeFromSuperview()
        self.commandView = nil
    }

}

extension FilterViewController : LocationSelectorDelegate {
    
    func tapAddLocation() {
        if let locationSelectionVC = self.storyboard?.instantiateViewController(withIdentifier: "LocationSelectionViewController") as? LocationSelectionViewController, let location = self.location {
            locationSelectionVC.location = location
            locationSelectionVC.delegate = self
            self.present(locationSelectionVC, animated: true, completion: nil)
        }
    }
    
    func tapLocationButton(venue : JSONParameters) {
        
        self.scrollviewLocation.subviews.forEach( { view in
            guard let viewLocation = view as? LocationView else {
                return
            }
            
            if let name = venue["name"] as? String {
                viewLocation.lblPlaceName?.text = name
            }
            
            if let c = venue["location"]?["city"] as? String
            ,let cc = venue["location"]?["country"] as? String {
                viewLocation.lblCountry?.text = "\(c) | \(cc)"
            }
            
            if viewLocation.backgroundPlace {
                viewLocation.setBackgroundToPlaceName()
            }
            
        })
    }
    
}

extension FilterViewController: LocationSelectionViewControllerDelegate {
    
    func onCancel() {
        
    }
    
    func onSelectVenus(venue : JSONParameters) {
        self.tapLocationButton(venue: venue)
    }
}
