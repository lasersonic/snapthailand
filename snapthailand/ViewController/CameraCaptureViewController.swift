//
//  CameraCaptureViewController.swift
//  snapthailand
//
//  Created by Appsynth on 3/29/2559 BE.
//  Copyright © 2559 Nutthawut. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import Async

struct FlashMode {
    let name : String!
    let image : String!
    let mode : AVCaptureFlashMode!
}

struct ShutterTimer {
    let name : String!
    let time : TimeInterval!
}

class CameraCaptureViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var constraintCaptureHeight: NSLayoutConstraint!
    @IBOutlet weak var captureView: UIView!
    @IBOutlet weak var btnFlash: UIButton!
    @IBOutlet weak var btnRatio: UIButton!
    @IBOutlet weak var btnSwapCamera: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnTimer: UIButton!
    @IBOutlet weak var btnGrid: UIButton!
    
    @IBOutlet weak var btnAlbum: UIButton!
    
    /* Private var */
    fileprivate let captureSession = AVCaptureSession()
    fileprivate var previewLayer : AVCaptureVideoPreviewLayer!
    
    fileprivate var frontCamera : AVCaptureDevice!
    fileprivate var backCamera : AVCaptureDevice!
    
    fileprivate var frontCameraInput : AVCaptureDeviceInput!
    fileprivate var backCameraInput : AVCaptureDeviceInput!
    fileprivate var currentInput : AVCaptureDeviceInput!
    fileprivate var stillImageOutput : AVCaptureStillImageOutput!
    
    fileprivate var flashModes = [
        FlashMode(name: "On", image: "flash-on", mode: .on)
        ,FlashMode(name: "Off", image: "flash-off", mode: .off)
        ,FlashMode(name: "Auto", image: "flash-auto", mode: .auto)]
    
    fileprivate var currentFlashMode = 0
    
    fileprivate var shutterModes = [
        ShutterTimer(name: "timer", time: 0),
        ShutterTimer(name: "timer-3", time: 3),
        ShutterTimer(name: "timer-10", time: 10)
    ]
    
    var lblTimerText : UILabel!
    
    fileprivate var currentShutterMode = 0
    
    var focusView: UIView?
    var gridView : GridView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupFocusView()
        
        setFlashMode()
        
        stillImageOutput = AVCaptureStillImageOutput()
        captureSession.sessionPreset = AVCaptureSessionPresetPhoto
        
        let devices = AVCaptureDevice.devices()
        
        for device in devices! {
            if ((device as AnyObject).hasMediaType(AVMediaTypeVideo)) {
                if((device as AnyObject).position == AVCaptureDevicePosition.back) {
                    backCamera = device as? AVCaptureDevice
                    backCameraInput = try! AVCaptureDeviceInput(device: backCamera)
                    currentInput = backCameraInput
                    
                }
                if((device as AnyObject).position == AVCaptureDevicePosition.front) {
                    frontCamera = device as? AVCaptureDevice
                    frontCameraInput = try! AVCaptureDeviceInput(device: frontCamera)
                }
            }
        }
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(didRotate(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getLastestImageAsGallery()
    }
    
    func shoudAutorotate() -> Bool {
        return false
    }
    
    func didRotate( _ notification : Notification) {
        let orientation = UIDevice.current.orientation
        rotateUI(orientation)
    }
    
    var previousRotation : UIDeviceOrientation = UIDeviceOrientation.portrait
    
    func rotateUI( _ orientation : UIDeviceOrientation ){
        
        var rotation = M_PI
        
        switch UIDevice.current.orientation {
            
        case .landscapeLeft: rotation = M_PI_2
        case .landscapeRight: rotation = -M_PI_2
        case .portraitUpsideDown: rotation = M_PI
        default: rotation = 0
        }
        
        UIView.animate(withDuration: 0.45, animations: {
            self.btnFlash.transform = CGAffineTransform(rotationAngle: CGFloat(rotation))
            self.btnRatio.transform = CGAffineTransform(rotationAngle: CGFloat(rotation))
            self.btnSwapCamera.transform = CGAffineTransform(rotationAngle: CGFloat(rotation))
            self.btnBack.transform = CGAffineTransform(rotationAngle: CGFloat(rotation))
            self.btnGrid.transform = CGAffineTransform(rotationAngle: CGFloat(rotation))
            self.btnTimer.transform = CGAffineTransform(rotationAngle: CGFloat(rotation))
        }) 
        
        self.previousRotation = orientation

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if backCamera != nil {
            beginSession()
        }
    }
    
    func setupFocusView(){
        self.focusView         = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        let tapRecognizer      = UITapGestureRecognizer(target: self, action: #selector(focus(_:)))
        tapRecognizer.delegate = self
        self.captureView.addGestureRecognizer(tapRecognizer)
    }
    
    func configureDevice() {
        if let device = backCamera {
            try! device.lockForConfiguration()
            device.focusMode = .continuousAutoFocus
            device.unlockForConfiguration()
        
            if backCamera.hasFlash && backCamera.isFlashModeSupported(.on) {
                try! device.lockForConfiguration()
                device.flashMode = .auto
                device.unlockForConfiguration()
            }
        }
    }
    
    func beginSession() {
        
        configureDevice()
        
        if captureSession.canAddInput(currentInput) {
            captureSession.addInput(currentInput)
            previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            self.captureView.layer.addSublayer(previewLayer)
            previewLayer?.frame = self.captureView.bounds
            previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            captureSession.startRunning()
        }
        
        if captureSession.canAddOutput(stillImageOutput) {
            captureSession.addOutput(stillImageOutput)
        }
    }
    
    func setFlashMode() {
        currentFlashMode = currentFlashMode == flashModes.count ? 0 : (currentFlashMode + 1) % flashModes.count
        
        let text = flashModes[currentFlashMode].image
        btnFlash.setImage(UIImage(named: text!), for: UIControlState())
        
        if let device = backCamera {
            try! device.lockForConfiguration()
            device.flashMode = flashModes[currentFlashMode].mode
            device.unlockForConfiguration()
        }
    }
    
    //MARK : - Focus
    
    func focus(_ recognizer: UITapGestureRecognizer) {
        
        let point = recognizer.location(in: self.view)
        let viewsize = self.captureView.frame.size
        let newPoint = CGPoint(x: point.y/viewsize.height, y: 1.0-point.x/viewsize.width)
        
        if let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo) {
        
            do {
                try device.lockForConfiguration()
            } catch _ {
                
                return
            }
            
            if device.isFocusModeSupported(AVCaptureFocusMode.autoFocus) == true {
                
                device.focusMode = AVCaptureFocusMode.autoFocus
                device.focusPointOfInterest = newPoint
            }
            
            if device.isExposureModeSupported(AVCaptureExposureMode.continuousAutoExposure) == true {
                
                device.exposureMode = AVCaptureExposureMode.continuousAutoExposure
                device.exposurePointOfInterest = newPoint
            }
            
            device.unlockForConfiguration()
            
            self.focusView?.alpha = 0.0
            self.focusView?.center = point
            self.focusView?.backgroundColor = UIColor.clear
            self.focusView?.layer.borderColor = UIColor.yellow.cgColor
            self.focusView?.layer.borderWidth = 1.0
            self.focusView!.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.view.addSubview(self.focusView!)
            
            UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.8,
                                       initialSpringVelocity: 3.0, options: UIViewAnimationOptions.curveEaseIn,
                animations: {
                    self.focusView!.alpha = 1.0
                    self.focusView!.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                }, completion: {(finished) in
                    self.focusView!.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    self.focusView!.removeFromSuperview()
            })
        }
    }

    //MARK : - Action
    
    @IBAction func shutterAction(_ sender: AnyObject) {
        
        if timerShutter != nil{
            return
        }
        
        let shutterTimerMode = shutterModes[currentShutterMode]
        
        if shutterTimerMode.time > 0 {
            animateDurationText()
        }
        
        Async.main(after: shutterTimerMode.time){ [weak self] in
            
            guard let strongSelf = self else { return }
            
            guard let connection = strongSelf.stillImageOutput.connection(withMediaType: AVMediaTypeVideo) else {
                return
            }
            
            strongSelf.stillImageOutput.captureStillImageAsynchronously(from: connection) { (buffer, error) in
                
                if let imageDataJpeg = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer) {
                    
                    var imageOrientation : UIImageOrientation!

                    switch UIDevice.current.orientation {
                        
                    case UIDeviceOrientation.portraitUpsideDown:
                        if strongSelf.currentInput == strongSelf.frontCameraInput {
                            imageOrientation = .rightMirrored
                        } else {
                            imageOrientation = .left
                        }
                    case UIDeviceOrientation.landscapeRight:
                        if strongSelf.currentInput == strongSelf.frontCameraInput {
                            imageOrientation = .up
                        } else {
                            imageOrientation = .down
                        }
                    case UIDeviceOrientation.landscapeLeft:
                        if strongSelf.currentInput == strongSelf.frontCameraInput {
                            imageOrientation = .down
                        } else {
                            imageOrientation = .up
                        }
                    case UIDeviceOrientation.portrait:
                        if strongSelf.currentInput == strongSelf.frontCameraInput {
                            imageOrientation = .leftMirrored
                        } else {
                            imageOrientation = .right
                        }
                    default:
                        imageOrientation = .up
                    }

                    //Rotate if need.
                    guard var pickedImage: UIImage = UIImage(data: imageDataJpeg) else {
                        return
                    }

                    let cropRect = CGRect(x: (pickedImage.size.height - pickedImage.size.width) / 2, y: 0, width: pickedImage.size.width, height: pickedImage.size.width)
                    
                    
                    if let imageRef = pickedImage.cgImage?.cropping(to: cropRect),
                        strongSelf.constraintCaptureHeight.multiplier == 1{
                        pickedImage = UIImage(cgImage: imageRef, scale: pickedImage.scale, orientation: pickedImage.imageOrientation)
                    }
                    
                    //We change orientation as imageOrientation changed.
                    pickedImage = UIImage(cgImage: pickedImage.cgImage!, scale: pickedImage.scale, orientation: imageOrientation)
                    //After that we fix to correct image orientation should be.
                    pickedImage = pickedImage.fixOrientationOfImage()!
                    //After that fix to up orientation
                    pickedImage = UIImage(cgImage: pickedImage.cgImage!, scale: pickedImage.scale, orientation: .up)
                    pickedImage = pickedImage.resizeUserImages()!

                    UIImageWriteToSavedPhotosAlbum(pickedImage, nil, nil, nil);
                    
                    if let filterVC = strongSelf.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController {
                        print(pickedImage.imageOrientation.rawValue)
                        filterVC.image = pickedImage
                        strongSelf.navigationController?.pushViewController(filterVC, animated: true)
                    }
                    
                }
            }
        }
    }
    
    var timerSecond = 0
    var timerShutter : Timer?
    
    func animateDurationText(){
        if lblTimerText == nil {
            lblTimerText = UILabel(frame: self.captureView.frame)
            lblTimerText.font = UIFont.systemFont(ofSize: 80)
            lblTimerText.textColor = UIColor.white
            lblTimerText.textAlignment = .center
            self.view.addSubview(lblTimerText)
        }
        
        timerSecond = Int(shutterModes[currentShutterMode].time)
        
        changeTextDurationText()
        
        timerShutter = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(changeTextDurationText), userInfo: nil, repeats: true)

    }
    
    func changeTextDurationText(){
        lblTimerText.text = String(format: "%d", timerSecond)
        timerSecond = timerSecond - 1
        
        if timerSecond < 0 {
            timerShutter?.invalidate()
            timerShutter = nil
            lblTimerText.removeFromSuperview()
            lblTimerText = nil
        }
    }
    
    @IBAction func flashAction(_ sender: UIButton) {
        setFlashMode()
    }

    @IBAction func ratioAction(_ sender: AnyObject) {
        
        self.captureView.layoutIfNeeded()
        self.view.layoutIfNeeded()

        if self.constraintCaptureHeight.multiplier == 1 {
            self.captureView.removeConstraint(self.constraintCaptureHeight)
            self.constraintCaptureHeight = NSLayoutConstraint(item: self.captureView, attribute: .width, relatedBy: .equal, toItem: self.captureView, attribute: .height, multiplier: CGFloat(3.0)/CGFloat(4.0), constant: 0)
            self.captureView.addConstraint(self.constraintCaptureHeight)
            self.btnRatio.setImage(UIImage(named: "ratio3-4"), for: UIControlState())
        }
        else{
            self.captureView.removeConstraint(self.constraintCaptureHeight)
            self.constraintCaptureHeight = NSLayoutConstraint(item: self.captureView, attribute: .width, relatedBy: .equal, toItem: self.captureView, attribute: .height, multiplier: 1, constant: 0)
            self.captureView.addConstraint(self.constraintCaptureHeight)
            self.btnRatio.setImage(UIImage(named: "ratio1-1"), for: UIControlState())
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.captureView.layoutIfNeeded()
            self.view.layoutIfNeeded()
            self.previewLayer?.frame = self.captureView.bounds
            self.gridView?.frame = self.captureView.bounds
        }, completion: { (success) in

        }) 

    }
    
    @IBAction func timerAction(_ sender: AnyObject) {
        currentShutterMode = (currentShutterMode + 1) % shutterModes.count
        let second = shutterModes[currentShutterMode].name
        self.btnTimer.setImage(UIImage(named: second!), for: UIControlState())
    }
    
    @IBAction func gridAction(_ sender: AnyObject) {
        
        if gridView == nil {
            gridView = GridView(frame: captureView.bounds)
            self.captureView.addSubview(gridView)
            gridView.isHidden = true
        }

        gridView.isHidden = !gridView.isHidden
        
        let imageName = gridView.isHidden ? "grid" : "grid-active"
        self.btnGrid.setImage(UIImage(named: imageName), for: UIControlState())
    }
    
    @IBAction func albumAction(_ sender: AnyObject) {
  
        let vc = FusumaViewController()
       
        let nav = UINavigationController()
        nav.viewControllers = [vc]

        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func switchDevices(_ sender: AnyObject) {
 
        captureSession.beginConfiguration()
        captureSession.removeInput(currentInput)
        
        if currentInput == backCameraInput {
            currentInput = frontCameraInput
        }
        else {
            currentInput = backCameraInput
        }
        
        captureSession.addInput(currentInput)
        captureSession.commitConfiguration()
        
    }
    
     @IBAction func pop(_ sender: AnyObject) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func getLastestImageAsGallery(){
        
        var firstImage : PHAsset?
        
        Async.background{
            let fetchOptions = PHFetchOptions()
            
            fetchOptions.sortDescriptors = [
                NSSortDescriptor(key: "creationDate", ascending: false)
            ]
          
          //  New
          if #available(iOS 9.0, *) {
            fetchOptions.includeAssetSourceTypes = .typeUserLibrary
          } else {
            // Fallback on earlier versions
          }
            
            let allPhotoResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
            
            var results = NSMutableArray()

            allPhotoResult.enumerateObjects(using: { (obj, idx, stop) in
                results.add(obj)
            })
          
          //  Old
//            let cameraRollAssets = (results as NSArray).filtered(using: NSPredicate(format: "assetSource == %@", argumentArray: [3]))
//            results = NSMutableArray(array: cameraRollAssets)
          
          //  New
            results = NSMutableArray(array: results)

            firstImage = results.firstObject as? PHAsset
        }
        .main {
            
            guard let firstImage = firstImage else {
                return
            }
            
            let options = PHImageRequestOptions()
            options.isSynchronous = false
            PHImageManager.default().requestImage(for: firstImage, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: options, resultHandler: { (image, info) in
                
                self.btnAlbum.setImage(image, for: UIControlState())
            })
        }
    }
}
