//
//  PhotoCropViewController.swift
//  snapthailand
//
//  Created by Nutthawut on 4/30/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit

class PhotoCropViewController: UIViewController {

    @IBOutlet weak var cropView: FSImageCropView!
    
    var image : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        self.cropView.isHidden = false
        
        cropView.imageSize = image?.size
        cropView.image = image
        
        cropView.changeScrollable(true)

    }
    

    @IBAction func backAction(_ sender: AnyObject) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cameraAction(_ sender: AnyObject) {
        
    }
    
    @IBAction func editAction(_ sender: AnyObject) {

        let view = cropView
        
        UIGraphicsBeginImageContextWithOptions((view?.frame.size)!, true, 0)
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: -(view?.contentOffset.x)!, y: -(view?.contentOffset.y)!)
        view?.layer.render(in: context!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
      
        let filterVC = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        filterVC.image = image
       
        self.navigationController?.pushViewController(filterVC, animated: true)
        
    }
}
