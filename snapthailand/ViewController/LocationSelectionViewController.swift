//
//  LocationSelectionViewController.swift
//  snapthailand
//
//  Created by Nutthawut on 4/17/17.
//  Copyright © 2017 Nutthawut. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationSelectionViewControllerDelegate: class {
    func onCancel()
    func onSelectVenus(venue : JSONParameters)
}

class LocationSelectionViewController : UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    weak var delegate : LocationSelectionViewControllerDelegate?
    
    fileprivate var venues : [JSONParameters] = []
    
    //MARK:- Public variable.
    
    var location : CLLocation = CLLocation(latitude: 37.332331412 , longitude	: -122.0312186)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.searchBar.delegate = self
        self.getLocation()
    }
    
    /**
     Get by location
     */
    func getLocation() {
        
        var locationName : String?
        
        if let text = self.searchBar.text, !text.isEmpty {
            locationName = text
        }
        
        GetProvinceService.requestLocation(self.location, name : locationName, limit: 50) { (success, venues) in
            if let venues = venues {
                self.venues = venues
                self.tableView.reloadData()
            }
        }
    }
    
    func reload() {
        getLocation()
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.delegate?.onCancel()
        self.dismiss(animated: true, completion: nil)
    }
}

extension LocationSelectionViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.venues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell") else {
            return UITableViewCell()
        }
        
        var name = ""
        var country = ""
        var city = ""
        
        let venue = self.venues[indexPath.row]
        
        if let n = venue["name"] as? String {
            name = n
        }
        if let c = venue["location"]?["city"] as? String {
            city = c
        }
        if let cc = venue["location"]?["country"] as? String {
            country = cc
        }
        
        cell.textLabel?.text = name
        cell.detailTextLabel?.text = "\(city), \(country)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let venue = self.venues[indexPath.row]
        self.delegate?.onSelectVenus(venue: venue)
        self.dismiss(animated: true, completion: nil)
    }
}

extension LocationSelectionViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(reload), object: nil)
        self.perform(#selector(reload), with: nil, afterDelay: 0.5)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(reload), object: nil)
        self.perform(#selector(reload), with: nil, afterDelay: 0.0)
    }
}
