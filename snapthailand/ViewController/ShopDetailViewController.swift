//
//  ShopDetailViewController.swift
//  snapthailand
//
//  Created by Nutthawut on 6/4/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyStoreKit
import Async

enum PurchaseState {
    case downloaded
    case purchased
    case none
}

struct DownloadStepText {
    static let stringDownloading = "Downloading"
    static let stringDownloadFree = "Download for free"
    static let stringDownloaded = "Downloaded"
}

class ShopDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var sticker : StickerItem?
    
    var purchased : Bool = false
    var purchaseState : PurchaseState = .none
    
    weak var delegate : ShopViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let sticker = sticker {
            
            let realm = try! Realm()
            
            //Check isPurchased
            let countOfId = realm.objects(PurchasedItem.self).filter(NSPredicate(format: "purchasedIds == %@ ", sticker.inapp_id!)).count
            let isPurchased = (countOfId == 1) ? true : false
            
            //Check Download?
            let query = NSPredicate(format: "stickerId == %@", sticker.inapp_id!)
            let isDownloaded = realm.objects(LocalSticker.self).filter(query).count > 0 ? true : false

            if isDownloaded {
                purchaseState = .downloaded
            }
            
            else if isPurchased {
                purchaseState = .purchased
            }
        }
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func purchaseSticker(_ button : UIButton){

        if purchaseState == .purchased || self.sticker?.price.lowercased() == "free" {
            self.downloadSticker()
            return
        }
        
        if !SwiftyStoreKit.canMakePayments {
            self.showAlert("Error", "Cannot make payment right now, please try again later")
            return
        }
        
        guard let stickerId = self.sticker?.sticker_id else {
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        if let inapp_id = sticker?.inapp_id {
            
            SwiftyStoreKit.purchaseProduct(inapp_id) { [weak self] (result) in
                
                guard let `self` = self else { return }
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch result {
                case .success(let product) :
                    print("product id was buying...")
                    
                    AnswerHelper.logStickerBuying(product.productId)

                    SendOrderPurchaseService.request( stickerId ) { (success) in
                        if success {
                            self.downloadSticker()
                        }
                    }
                    
                case .error(let error):
                    self.showAlert("Error", error.localizedDescription)
                }
            }
        }
    }
    
    func downloadSticker(){
        
        let progressView = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressView?.mode = .annularDeterminate
        progressView?.labelText = DownloadStepText.stringDownloading
        
        DownloadManager.sharedInstance.download(self.sticker!, progressBlock: { progress in
        
            Async.main {
                progressView?.progress = progress
            }
        
        }, successBlock:  { [weak self] in
            
            progressView?.hide(true)
            
            if let cell = self?.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? StickerListHeaderCell {
                cell.btnBuy.isEnabled = false
                cell.btnBuy.setTitle(DownloadStepText.stringDownloaded, for: UIControlState())
                cell.btnBuy.backgroundColor = UIColor.lightGray
            }
            
            self?.delegate?.reload()

            
        })
    }
    
   
}

extension ShopDetailViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 200
        }
        return 90
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let stickerList = sticker?.stickerLists {
            let count = Int(ceil(Float(stickerList.count) / 4.0))
            return count + 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "StickerListHeaderCell") as! StickerListHeaderCell
            guard let sticker = sticker else { return cell }
            
            cell.lblTitle.text = sticker.name
            cell.lblDetail.text = sticker.detail
            let imageURLString = sticker.avatar
            let imageURL = URL(string: imageURLString!)!
            cell.imageThumb.sd_setImage(with: imageURL)
            cell.btnBuy.addTarget(self, action: #selector(purchaseSticker(_:)), for: .touchUpInside)
            
            let stringBuy = sticker.price

            if purchaseState == .downloaded {
                cell.btnBuy.isEnabled = false
                cell.btnBuy.setTitle(DownloadStepText.stringDownloaded, for: UIControlState())
                cell.btnBuy.backgroundColor = UIColor.lightGray
            }
            else if purchaseState == .purchased || stringBuy.lowercased() == "free"{
                cell.btnBuy.setTitle(DownloadStepText.stringDownloadFree, for: UIControlState())
            }
            else {
                cell.btnBuy.setTitle("Buy (\(stringBuy))", for: UIControlState())
                
            }
            
            return cell
        }
        
        let stickerCell = tableView.dequeueReusableCell(withIdentifier: "StickerListCell") as! StickerListCell
        
        let index = (indexPath.row - 1) * 4
        
        for i in 0..<stickerCell.imageViewCollection.count {
            let imageView = stickerCell.imageViewCollection[i]
            if index + i >= sticker!.thumbnails!.count {
                imageView.image = nil
            }else{
                let imageURLString = sticker!.thumbnails![index + i]
                let imageURL = URL(string: imageURLString)!
                imageView.sd_setImage(with: imageURL)
            }
        }

        return stickerCell
    }

}
