//
//  FacebookService.swift
//  snapthailand
//
//  Created by Nutthawut on 4/8/17.
//  Copyright © 2017 Nutthawut. All rights reserved.
//

import Alamofire

class FacebookService : NSObject{
    
    class func request(_ fbid : String, email : String, firstName : String, lastName : String, callback : @escaping (_ success : Bool) -> Void) {
        
        let requestUrl = String(format:"%@/api/v1/facebook/saveuser", API.baseurl)
        
        let data : [AnyHashable: Any] = ["fbid": fbid
            , "email" : email
            , "first_name" : firstName
            , "last_name" : lastName]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: [])
            let string = String(data: jsonData, encoding: String.Encoding.utf8)!
            let param = ["data" : string]
            
            let request = ASRequest(method: .post, url: requestUrl , parameter: param)
            
            API.sharedInstance.sendRequest(request) { (base : BaseModel?, error : Error?) in
                callback(error == nil)
            }
            
        } catch let error as NSError {
            print(error)
        }
        
        
    }
    
}
