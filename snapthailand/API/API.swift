//
//  API.swift
//  AppsynthParse
//
//  Created by Appsynth on 3/21/2559 BE.
//  Copyright © 2559 Appsynth. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

struct ASRequest {
    let method: HTTPMethod
    let url : String!
    let parameter : [String : String]?
}


class API : NSObject {
    
    static var baseurl = "http://snapthailand.rgb72.net"
    
    static var sharedInstance = API()
    
    override init() {
        
    }
    
    class func start() {
        sharedInstance = API()
    }

    
    func sendRequest<T:Mappable>(_ request : ASRequest, callback:@escaping (T?, Error?) -> Void){
        
        let url = URL(string : request.url)!
        
        var encoding = URLEncoding.default
        if request.method == .post {
            encoding = .httpBody
        }
        
        
        Alamofire.request(url, method: request.method, parameters: request.parameter, encoding: encoding, headers: nil).responseObject { (res : DataResponse<T>) in
            callback(res.result.value, res.result.error)
        }
        
    }

}


