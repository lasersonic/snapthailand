//
//  GetStickerService.swift
//  snapthailand
//
//  Created by Appsynth on 4/26/2559 BE.
//  Copyright © 2559 Nutthawut. All rights reserved.
//

import UIKit
import Alamofire

class GetStickerService: NSObject {

    class func requestShop(_ callback : @escaping (_ stickers : [StickerItem]?) -> Void) {
        
        let requestUrl = String(format:"%@/api/v1/sticker-collections", API.baseurl)
        
        let request = ASRequest(method: .get, url: requestUrl , parameter: nil)
        API.sharedInstance.sendRequest(request) { (sticker : Sticker?, error : Error?) in
            
            var stickers : [StickerItem]?
            
            if let s = sticker {
                stickers = s.stickers
            }
            
            callback(stickers)
        }
    }
    
    
}
