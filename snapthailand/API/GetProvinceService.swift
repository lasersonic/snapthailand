//
//  GetProvinceService.swift
//  snapthailand
//
//  Created by Nutthawut on 5/24/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit
import Alamofire
import QuadratTouch
import CoreLocation

typealias JSONParameters = [String: AnyObject]

class GetProvinceService: NSObject {
    
    
    class func request(_ location : CLLocation, _ callback : @escaping (_ success : Bool, _ venue : [JSONParameters]?) -> Void) {
        
        let session = Session.sharedSession()
        var parameters =  location.parameters()
        parameters += ["limit" : "5", "v" : "20160611", "locale" : "en"]
        let searchTask = session.venues.search(parameters) {
            (result) -> Void in
            if let response = result.response, let v = response["venues"] as? [JSONParameters], v.count > 0 {
                callback(true, v)
            }
            else {
                callback(false, nil)
            }
        }
        searchTask.start()

    }

    class func requestLocation(_ location : CLLocation, name: String?, limit: Int, _ callback : @escaping (_ success : Bool, _ venue : [JSONParameters]?) -> Void) {
        
        let session = Session.sharedSession()
        var parameters =  location.parameters()
        
        parameters += ["limit" : String(limit), "v" : "20160611", "locale" : "en"]
        
        if let name = name {
            parameters += ["query" : name]
        }
        
        let searchTask = session.venues.search(parameters) {
            (result) -> Void in
            if let response = result.response, let v = response["venues"] as? [JSONParameters] {
                callback(true, v)
            }
            else {
                callback(false, nil)
            }
        }
        searchTask.start()
        
    }
    
}

extension CLLocation {
    func parameters() -> QuadratTouch.Parameters {
        let ll      = "\(self.coordinate.latitude),\(self.coordinate.longitude)"
        let llAcc   = "\(self.horizontalAccuracy)"
        let alt     = "\(self.altitude)"
        let altAcc  = "\(self.verticalAccuracy)"
        let parameters = [
            Parameter.ll:ll,
            Parameter.llAcc:llAcc,
            Parameter.alt:alt,
            Parameter.altAcc:altAcc
        ]
        return parameters
    }
}
