//
//  User.swift
//  AppsynthParse
//
//  Created by Appsynth on 3/22/2559 BE.
//  Copyright © 2559 Appsynth. All rights reserved.
//

import UIKit
import ObjectMapper

class Sticker: BaseModel {

    var stickers : [StickerItem]?
 
    override func mapping(map: Map) {
        super.mapping(map: map)
        stickers   <- map["data"]
    }
}

class StickerItem : Mappable {
    
    var sticker_id : Int?
    var inapp_id : String?
    var name : String?
    var detail : String?
    var avatar : String?
    var preview_thumbnails : [String]?
    var stickerLists : [String]?
    var thumbnails : [String]?
    var download_link : String?
    var price : String = ""
    var orderSequence : String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        sticker_id <- map["id"]
        inapp_id <- map["ios_inapp_id"]
        name <- map["name"]
        detail <- map["description"]
        download_link <- map["download_link"]
        avatar <- map["avatar"]
        preview_thumbnails <- map["preview_thumbnails"]
        thumbnails <- map["thumbnails"]
        stickerLists <- map["stickerLists"]
        orderSequence <- map["order_sequence"]
    }
    
}
