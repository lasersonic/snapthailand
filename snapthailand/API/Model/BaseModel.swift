//
//  BaseModel.swift
//  AppsynthParse
//
//  Created by Appsynth on 3/23/2559 BE.
//  Copyright © 2559 Appsynth. All rights reserved.
//

import ObjectMapper

class BaseModel: Mappable {

    var code : Int?
    var message : String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        code <- map["meta.status_code"]
        message <- map["meta.message"]
    }
}
