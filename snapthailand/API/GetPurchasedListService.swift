//
//  GetPurchasedListService.swift
//  snapthailand
//
//  Created by Nutthawut on 7/11/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import Alamofire

class GetPurchasedListService : NSObject{
    
    class func request(_ stickerIds : [String], callback : (_ success : Bool) -> Void) {
        
        let requestUrl = String(format:"%@/api/v1/sticker-collections/purchased", API.baseurl)
        
        let data = ["stickerIds": stickerIds]
        
        let param = ["data" : data.description]
        
        let request = ASRequest(method: .get, url: requestUrl, parameter: param)
        API.sharedInstance.sendRequest(request) { (base : BaseModel?, error : Error?) in
            //ignore response.
        }
    }
    
}
