//
//  SendOrderPurchase.swift
//  snapthailand
//
//  Created by Nutthawut on 7/3/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import Alamofire
import SwiftKeychainWrapper

class SendOrderPurchaseService : NSObject{
    
    class func request(_ stickerId : Int, callback : @escaping (_ success : Bool) -> Void) {
        
        let requestUrl = String(format:"%@/api/v1/orders/purchase", API.baseurl)

        let device = UIDevice.current
        
        var device_id = KeychainWrapper.standard.string(forKey: DeviceIdKey)
        if device_id == nil {
            device_id = UIDevice.current.identifierForVendor!.uuidString
        }

        let data : [AnyHashable: Any] = ["sticker_collection_id": NSNumber(value: stickerId), "platform": "ios", "device_id": device_id! , "device_name" : device.name, "os_version" : device.systemVersion]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: [])
            let string = String(data: jsonData, encoding: String.Encoding.utf8)!
            let param = ["data" : string]
            
            let request = ASRequest(method: .post, url: requestUrl , parameter: param)
            
            API.sharedInstance.sendRequest(request) { (base : BaseModel?, error : Error?) in
                callback(error == nil)
            }

        } catch let error as NSError {
            print(error)
        }
        

    }

}
