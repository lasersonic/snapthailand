//
//  LocationView.swift
//  snapthailand
//
//  Created by Nutthawut on 6/11/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit

class LocationView: UIView {
    
    @IBInspectable var isCenter : Bool = false
    @IBInspectable var isStrechHorizontal : Bool = true
    @IBInspectable var backgroundPlace : Bool = false

    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblPlaceName: UILabel!
        
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        super.draw(rect)
        
        if backgroundPlace {
            setBackgroundToPlaceName()
        }
        
    }
    
    func setBackgroundToPlaceName() {
        if let text = lblPlaceName.text {
            let s = NSMutableAttributedString(string: text)
            s.addAttributes([NSBackgroundColorAttributeName :  UIColor.black], range: NSMakeRange(0, s.length))
            self.lblPlaceName.attributedText = s
        }
    }
 

}
