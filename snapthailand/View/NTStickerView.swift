//
//  NTStickerView.swift
//  snapthailand
//
//  Created by Nutthawut on 7/10/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import Foundation

protocol NTStickerViewDelegate: class {
    func didSelectSticker( _ sticker : NTStickerView )
    func didDoubleTap(_ sticker : NTStickerView)
}

class NTStickerView : UIView {
    
    weak var delegate : NTStickerViewDelegate?
    var userData : [AnyHashable: Any] = [:]
    var isText : Bool = false
    var isStateSelected : Bool = false {
        didSet {
            if isStateSelected {
                showBorder()
            }else{
                hideBorder()
            }
        }
    }
    //hierrachy is
    // root - wrapperView
    //       |- containerView
    //          | - contentView
    //              |- imageView
    
    var wrapperView : UIView!
    var containerView : UIView!
    var contentView : UIView! {
        
        willSet {
            if contentView != nil {
                contentView.removeFromSuperview()
            }
        }
        
        didSet {
            
            wrapperView.frame = self.bounds
            
            containerView.frame = self.wrapperView.bounds.insetBy(dx: resizeButtonSize/2, dy: resizeButtonSize/2)
            contentView.frame = containerView.bounds
            
            contentView.backgroundColor = UIColor.clear
            
            containerView.addSubview(contentView)
            
            setPositionResizeControl()
        }
    }
    
    var lastZoomScale : CGFloat = 1.0
    
    let minimumSize = CGSize(width: 60, height: 60)
    
    //Resize Control.
    let resizeButtonSize : CGFloat = 30.0
    static let BUTTONSIZE : CGFloat = 30.0
    
    var topLeft : UIView!
    var topRight : UIView!
    var bottomLeft : UIView!
    var bottomRight : UIView!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        wrapperView = UIView(frame: self.bounds)
        
        containerView = UIView(frame: self.bounds.insetBy(dx: resizeButtonSize/2, dy: resizeButtonSize/2))
        
        self.addSubview(wrapperView)
        wrapperView.addSubview(containerView)
        
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchGestureAction(_:)))
        pinchGesture.delegate = self
        self.addGestureRecognizer(pinchGesture)
        
        let rotateGesture = UIRotationGestureRecognizer(target: self, action: #selector(rotateGestureAction(_:)))
        rotateGesture.delegate = self
        self.addGestureRecognizer(rotateGesture)
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(doubleTapGesture(_:)))
        doubleTap.numberOfTapsRequired = 2
        self.addGestureRecognizer(doubleTap)
    
        setupResizeControl()

        showBorder()
    }
    
    func setupResizeControl() {
        topLeft = UIView()
        topRight = UIView()
        bottomLeft = UIView()
        bottomRight = UIView()

        setPositionResizeControl()

        //define real size of layer but portion of `resizeButtonSize`
        let layerFrame = topLeft.bounds.insetBy(dx: resizeButtonSize/3, dy: resizeButtonSize/3)
        
        let rect = CAShapeLayer()
        rect.backgroundColor = UIColor.white.cgColor
        rect.frame = layerFrame
        rect.borderWidth = 1.0 / UIScreen.main.scale
        rect.borderColor = UIColor.darkGray.cgColor
        
        let rect2 = CAShapeLayer()
        rect2.backgroundColor = UIColor.white.cgColor
        rect2.frame = layerFrame
        rect2.borderWidth = 1.0 / UIScreen.main.scale
        rect2.borderColor = UIColor.darkGray.cgColor
        
        let rect3 = CAShapeLayer()
        rect3.backgroundColor = UIColor.white.cgColor
        rect3.frame = layerFrame
        rect3.borderWidth = 1.0 / UIScreen.main.scale
        rect3.borderColor = UIColor.darkGray.cgColor
        
        let rect4 = CAShapeLayer()
        rect4.backgroundColor = UIColor.white.cgColor
        rect4.frame = layerFrame
        rect4.borderWidth = 1.0 / UIScreen.main.scale
        rect4.borderColor = UIColor.darkGray.cgColor
        
        topLeft.layer.addSublayer(rect)
        topRight.layer.addSublayer(rect2)
        bottomLeft.layer.addSublayer(rect3)
        bottomRight.layer.addSublayer(rect4)

        wrapperView.addSubview(topLeft)
        wrapperView.addSubview(topRight)
        wrapperView.addSubview(bottomLeft)
        wrapperView.addSubview(bottomRight)
    }
    
    func setPositionResizeControl() {
        topLeft.frame = CGRect(x: 0, y: 0, width: resizeButtonSize, height: resizeButtonSize)
        topRight.frame = CGRect(x: self.wrapperView.bounds.width - resizeButtonSize, y: 0, width: resizeButtonSize, height: resizeButtonSize)
        bottomLeft.frame = CGRect(x: 0, y: self.wrapperView.bounds.height - resizeButtonSize, width: resizeButtonSize, height: resizeButtonSize)
        bottomRight.frame = CGRect(x: self.wrapperView.bounds.width - resizeButtonSize, y: self.wrapperView.bounds.height - resizeButtonSize, width: resizeButtonSize, height: resizeButtonSize)
    }
    
    func hidePositionResizeControl(_ hide : Bool) {
        topLeft.isHidden = hide
        topRight.isHidden = hide
        bottomLeft.isHidden = hide
        bottomRight.isHidden = hide
    }
    
    
    func doubleTapGesture(_ recognizer : UITapGestureRecognizer){
        self.delegate?.didDoubleTap(self)
    }
    
    func rotateGestureAction(_ recognizer : UIRotationGestureRecognizer){
        if let view = recognizer.view {
            view.transform = view.transform.rotated(by: recognizer.rotation)
            recognizer.rotation = 0
        }
    }
    
    var oldCenter : CGPoint!
    
    func pinchGestureAction(_ recognizer : UIPinchGestureRecognizer){
        
        guard let _ = recognizer.view else {
            return
        }
        
        if recognizer.state == .began {
            oldCenter = self.center
            lastZoomScale = recognizer.scale
        }
        
        let insetFactor = recognizer.scale - 1
        let newWidth = self.bounds.width + self.bounds.width * insetFactor
        let newHeight = self.bounds.height + self.bounds.height * insetFactor

        let newBounds = CGRect(x: 0, y: 0, width: newWidth, height: newHeight)
        
        recognizer.scale = 1
        
        if newBounds.width < minimumSize.width || newBounds.height < minimumSize.height {
            return
        }
        
        wrapperView.frame = newBounds
        self.bounds = newBounds
        self.center = oldCenter
        
        containerView.frame = newBounds.insetBy(dx: resizeButtonSize/2, dy: resizeButtonSize/2)
        contentView.frame = containerView.bounds
        
        setPositionResizeControl()
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        delegate?.didSelectSticker(self)
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        
        guard let touch = touches.first else {
            return
        }
        
        if touch.view == topLeft ||
            touch.view == topRight ||
            touch.view == bottomLeft ||
            touch.view == bottomRight {


            let loc = touch.location(in: touch.view)
            let pLoc = touch.previousLocation(in: touch.view)

            let translateX : CGFloat = loc.x - pLoc.x
            let translateY : CGFloat = loc.y - pLoc.y
            
            var newOrigin : CGPoint!
            var newSize : CGSize!
            
            if touch.view == topLeft {
                newOrigin = CGPoint(x: self.wrapperView.frame.origin.x + translateX, y: self.wrapperView.frame.origin.y + translateY)
                newSize = CGSize(width: self.wrapperView.frame.size.width - translateX, height: self.wrapperView.frame.size.height - translateY)
            } else if touch.view == topRight {
                newOrigin = CGPoint(x: self.wrapperView.frame.origin.x, y: self.wrapperView.frame.origin.y + translateY)
                newSize = CGSize(width: self.wrapperView.frame.size.width + translateX, height: self.wrapperView.frame.size.height - translateY)
            } else if touch.view == bottomLeft {
                newOrigin = CGPoint(x: self.wrapperView.frame.origin.x + translateX, y: self.wrapperView.frame.origin.y)
                newSize = CGSize(width: self.wrapperView.frame.size.width  - translateX, height: self.wrapperView.frame.size.height + translateY)
            } else if touch.view == bottomRight {
                newOrigin = CGPoint(x: self.wrapperView.frame.origin.x, y: self.wrapperView.frame.origin.y)
                newSize = CGSize(width: self.wrapperView.frame.size.width + translateX, height: self.wrapperView.frame.size.height + translateY)
            }
            
            if newSize.width < minimumSize.width || newSize.height < minimumSize.height {
                return
            }
            
            self.wrapperView.bounds = CGRect(origin: CGPoint.zero, size: newSize)
            self.wrapperView.center = CGPoint(x: CGRect(origin: newOrigin, size: newSize).midX, y: CGRect(origin: newOrigin, size: newSize).midY)
            
            containerView.frame = self.wrapperView.bounds.insetBy(dx: resizeButtonSize/2, dy: resizeButtonSize/2)
            contentView.frame = containerView.bounds

            setPositionResizeControl()
            
            let frame = self.convert(wrapperView.frame, to: self.superview)
            self.bounds = wrapperView.bounds
            self.center = CGPoint(x: frame.midX, y: frame.midY)
            wrapperView.frame.origin = self.bounds.origin
            
            return
        }
        
        let location = touch.location(in: self.superview!)
        let previousLocation = touches.first!.previousLocation(in: self.superview!)
        
        let x = location.x - previousLocation.x
        let y = location.y - previousLocation.y

        let newCenter = CGPoint(x: self.center.x + x, y: self.center.y + y)
        self.center =  newCenter
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
    }
    
    func showBorder(){
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor
        self.containerView.layer.borderWidth = 1
        hidePositionResizeControl(false)
    }
    
    func hideBorder(){
        self.containerView.layer.borderColor = UIColor.clear.cgColor
        self.containerView.layer.borderWidth = 0
        hidePositionResizeControl(true)
    }
    
}

extension NTStickerView : UIGestureRecognizerDelegate{
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
