//
//  NTLabel.swift
//  snapthailand
//
//  Created by Nutthawut on 10/2/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit

@IBDesignable
class NTLabel: UILabel {
    
    @IBInspectable var fontSize : CGFloat = 22
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.font = UIFont(name: "2006iannnnnBKK", size: fontSize)

    }
}
