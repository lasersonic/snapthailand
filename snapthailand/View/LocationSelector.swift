//
//  LocationSelector.swift
//  snapthailand
//
//  Created by Nutthawut on 4/20/17.
//  Copyright © 2017 Nutthawut. All rights reserved.
//

import Foundation

protocol LocationSelectorDelegate : class {
    func tapAddLocation()
    func tapLocationButton(venue : JSONParameters)
}
class LocationSelector : UIView {
    
    @IBOutlet var locationButtons: [UIButton]!
    @IBOutlet weak var btnAddLocation: UIButton!
    
    weak var delegate: LocationSelectorDelegate?
    
    var venues : [JSONParameters]! {
        didSet {
            
            let venuesCount = venues.count
            
            for index in 0 ..< self.locationButtons.count {
                
                let button = self.locationButtons[index]

                if index >= venuesCount {
                    button.removeFromSuperview()
                } else {
                    button.addTarget(self, action: #selector(locationAction(button:)), for: .touchUpInside)
                    
                    if let name = venues[index]["name"] as? String {
                        button.setTitle(name, for: .normal)
                    }
                }
                
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    @IBAction func addLocationAction(_ sender: Any) {
        self.delegate?.tapAddLocation()
    }
    
    func locationAction(button : UIButton) {
        if let index = locationButtons.index(of: button) {
            self.delegate?.tapLocationButton(venue: venues[index])
        }
    }
    
}
