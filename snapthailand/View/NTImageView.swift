//
//  NTImage.swift
//  snapthailand
//
//  Created by Nutthawut on 8/28/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit

@IBDesignable
class NTImageView: UIImageView {
    
    @IBInspectable
    var flip : Bool = false {
        didSet {
            if !flip {
                return
            }
            guard let image = self.image else { return }
            self.image = UIImage(cgImage: image.cgImage!, scale : image.scale, orientation : .upMirrored)
        }
    }

}
