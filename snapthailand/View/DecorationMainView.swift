//
//  DecorationMainView.swift
//  snapthailand
//
//  Created by Nutthawut on 5/26/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit

class DecorationMainView: UIView {

    
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnEffect: UIButton!
    @IBOutlet weak var btnFrame: UIButton!
    @IBOutlet weak var btnText: UIButton!
    @IBOutlet weak var btnSticker: UIButton!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var btnExpand: UIButton!
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        setupButton(self.btnFilter)
    }
    
    func setupButton(_ button: UIButton) {
        let spacing: CGFloat = 6.0
        let imageSize: CGSize = button.imageView!.image!.size
        button.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0);
        let labelString = NSString(string: button.currentTitle!)
        let titleSize = labelString.size(attributes: [NSFontAttributeName: button.titleLabel!.font])
        button.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width);
    }
}
