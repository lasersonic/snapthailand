//
//  GridView.swift
//  snapthailand
//
//  Created by Nutthawut on 4/26/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit

class GridView: UIView {

    var numberOfColumns : Int = 2
    var numberOfRows : Int = 2
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clear

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        let context = UIGraphicsGetCurrentContext()
        context?.setLineWidth(0.5)
        context?.setStrokeColor(UIColor.white.cgColor)
        
        // ---------------------------
        // Drawing column lines
        // ---------------------------
        
        // calculate column width
        let columnWidth = self.frame.size.width / CGFloat(self.numberOfColumns + 1)
        
        for i in 1...self.numberOfColumns
        {
            var startPoint = CGPoint.zero
            var endPoint = CGPoint.zero
            
            startPoint.x = columnWidth * CGFloat( i )
            startPoint.y = 0.0
            
            endPoint.x = startPoint.x
            endPoint.y = self.frame.size.height
            
            context?.move(to: CGPoint(x: startPoint.x, y: startPoint.y))
            context?.addLine(to: CGPoint(x: endPoint.x, y: endPoint.y))
            context?.strokePath()
        }
        
        
        // ---------------------------
        // Drawing row lines
        // ---------------------------
        
        // calclulate row height
        let rowHeight = self.frame.size.height / CGFloat(self.numberOfRows + 1);
        
        for j in 1...self.numberOfRows
        {
            var startPoint = CGPoint.zero
            var endPoint = CGPoint.zero
            
            startPoint.x = 0
            startPoint.y = rowHeight * CGFloat(j);
            
            endPoint.x = self.frame.size.width;
            endPoint.y = startPoint.y;
            
            context?.move(to: CGPoint(x: startPoint.x, y: startPoint.y))
            context?.addLine(to: CGPoint(x: endPoint.x, y: endPoint.y))
            context?.strokePath()
        }

    }
}
