//
//  NTProgressBar.swift
//  snapthailand
//
//  Created by Nutthawut on 6/19/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit
import QuartzCore

enum ThumbState {
    case start
    case stop
}

protocol NTProgressBarDelegate : class {
    func onNTProgress(_ value : CGFloat, showingValue : CGFloat, valueProgress : CGFloat)
    func onNTThumbMove(_ progressBar : NTProgressBar, thumb : CGPoint)
    func onNTThumbState( _ progressBar : NTProgressBar, state : ThumbState )
}

class NTProgressBar: PivotSlider {
    
    weak var delegate : NTProgressBarDelegate?
    
    
    var slideAction : SlideAction? {
        willSet {
            guard let slideValue = newValue else {
                return
            }
            switch slideValue {
            case .slideFilterIntensity, .slideWhite, .slideAdjustFontOpacity:
                self.minimumValue = 0
                self.maximumValue = 100
                self.pivotValue = 0
                self.circlePointView.isHidden = true
            default:
                self.minimumValue = -50
                self.maximumValue = 50
                self.pivotValue = 0
                self.circlePointView.isHidden = false
            }
        }
    }
    
    var filterValue : Float {
        return filterValueByProgressBarValue(self.value)
    }
    
    //MARK: - Initialize Custom values
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.track.backgroundColor = ColorHelper.progressBarBackgroundColor
        self.valueTrack.backgroundColor = ColorHelper.progressBarProgressColor
        self.isContinuous = false
    }
    
    required public init(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    //MARK: - Handle touches.
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if let windowPoint = self.thumb.superview?.convert(self.thumb.center, to: nil) {
            self.delegate?.onNTThumbMove(self, thumb: windowPoint)
        }
        self.delegate?.onNTThumbState(self, state: .start)
    }
    
    override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let _ =  super.continueTracking(touch, with: event)
        if let windowPoint = self.thumb.superview?.convert(self.thumb.center, to: nil) {
            self.delegate?.onNTThumbMove(self, thumb: windowPoint)
        }
        
        let filterValue = filterValueByProgressBarValue(self.value)
        
        self.delegate?.onNTProgress(CGFloat(filterValue), showingValue: CGFloat(self.value), valueProgress: CGFloat(self.value))
        return true
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.delegate?.onNTThumbState(self, state: .stop)
    }
    
    //MARK: - Calculate value method
    //Progress bar value can be in range -50 <-> 50 or 0 <-> 1
    func filterValueByProgressBarValue(_ value : Float) -> Float{
        
        guard let slideAction = self.slideAction else {
            return 0
        }
        switch slideAction {
        case .slideFilterIntensity:
            return value / 100.0
        case .slideBrightness:
            return value / 50.0 / 5.0
        case .slideContrast:
            if (value < 0) {
                return 1 - abs(value / 75.0)
            }
            else {
                return 1 + (value / 75.0) * 3.0
            }
        case .slideSaturation:
            return value / 50 + 1
        case .slideShapen:
            return value / 12.5
        case .slideWhite, .slideAdjustFontOpacity:
            return 0.2 + (0.8 * (value / 100.0))
        default: return 0
        }
    }
    
}
