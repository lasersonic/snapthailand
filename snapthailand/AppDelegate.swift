//
//  AppDelegate.swift
//  snapthailand
//
//  Created by Appsynth on 3/28/2559 BE.
//  Copyright © 2559 Nutthawut. All rights reserved.
//

import UIKit
import QuadratTouch
import Alamofire
import Fabric
import Crashlytics
import FBSDKCoreKit
import RealmSwift
import TwitterKit

let newSchemeVersion : UInt64 = 2

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        application.isStatusBarHidden = true
        
        let client = Client(clientID:       "3MKXXACLJG5OCTBVGFZC5WJ3RUHDQZFSYHHTOIM3B0WTOSJV",
                            clientSecret:   "ZCVD2JMS0SFHA2BASTX1DD0CBW0DKVEDTTFUCYXXEQ1EHPI5",
                            redirectURL:    "testapp123://foursquare")
        let configuration = Configuration(client:client)
        Session.setupSharedSessionWithConfiguration(configuration)

        //MARK:- List all Fonts
//        for a in UIFont.familyNames() {
//            print(a)
//            for b in UIFont.fontNamesForFamilyName(a) {
//                print("---\(b)")
//            }
//        }
        
        //MARK:- Fabric Stuff
        Fabric.with([Answers.self, Crashlytics.self])
        
        //MARK:- Facebook Stuff
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        let config = Realm.Configuration(schemaVersion: newSchemeVersion, migrationBlock: { migration, oldSchemaVersion in
            
            if oldSchemaVersion < newSchemeVersion {
                migration.enumerateObjects(ofType: LocalSticker.className()) { oldObject, newObject in
                    newObject!["stickerThumbnailIndex"] = 0
                }
            }
        })
      
        //  MARK: - Twitter Stuff
        Twitter.sharedInstance().start(withConsumerKey: "QvftkdUixHyOawDcjqYggepUo", consumerSecret: "V9Hx4FlMPLxPQsEjMhnhq23nwVyw1YnZeshYHdxMsouRo6iXQ1")
        
        Realm.Configuration.defaultConfiguration = config
        
        return true
    }

    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
  
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
      if Twitter.sharedInstance().application(app, open: url, options: options) {
        return true
      } else if FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options) {
        return true
      } else {
        return false
      }
    }
  
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

