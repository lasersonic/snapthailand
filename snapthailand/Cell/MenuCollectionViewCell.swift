//
//  MenuCollectionViewCell.swift
//  snapthailand
//
//  Created by Nutthawut on 5/30/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var lblName: UILabel!
 
    var item : PlistItem!
    
    func setCell( _ item : PlistItem ){
        self.item = item
        lblName.text = item.name
        thumbnail.image = item.image
    }
    
    override var isSelected: Bool {
        didSet {
            lblName?.textColor = isSelected ? ColorHelper.selectedMenuColor : UIColor.darkGray
            thumbnail?.image = isSelected ? item.imageActived : item.image
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        lblName.sizeToFit()
    }
}
