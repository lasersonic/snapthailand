//
//  FilterCollectionCell.swift
//  snapthailand
//
//  Created by Appsynth on 3/29/2559 BE.
//  Copyright © 2559 Nutthawut. All rights reserved.
//

import UIKit

class FilterCollectionCell: UICollectionViewCell {

    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var lblFilterName: UILabel!
    
    @IBOutlet weak var labelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelConstraint: NSLayoutConstraint!
    
    var enabled : Bool = true
    
    var item : PlistItem?

    override func prepareForReuse() {
        super.prepareForReuse()
        self.item = nil
        labelConstraint?.isActive = true
        labelHeightConstraint?.constant = 21
        isSelected = false
    }
    
    func setCellWithActiveStatus( _ item : PlistItem?, editingStatus: (Bool,Bool), progressValue:[SlideAction : CGFloat] ) {

        guard let item = item else { return }
        
        self.setCell(item)
        
        if item.name.lowercased() == "crop" {
            self.isSelected = editingStatus.0
        }
        else if item.name.lowercased() == "rotate" {
            self.isSelected = editingStatus.1
        }
        else if item.name.lowercased() == "brightness" {
            if let brightness = progressValue[SlideAction.slideBrightness], Int(brightness) != 0 {
                self.isSelected = true
            }
        }
        else if item.name.lowercased() == "contrast" {
            if let contrast = progressValue[SlideAction.slideContrast], Int(contrast) != 0 {
                self.isSelected = true
            }
        }
        else if item.name.lowercased() == "saturation" {
            if let saturation = progressValue[SlideAction.slideSaturation], Int(saturation) != 0 {
                self.isSelected = true
            }
        }
        else if item.name.lowercased() == "sharpen" {
            if let sharpen = progressValue[SlideAction.slideShapen], Int(sharpen) != 0 {
                self.isSelected = true
            }
        }
        else if item.name.lowercased() == "fade" {
            if let slideWhite = progressValue[SlideAction.slideWhite], Int(slideWhite) < 100 {
                self.isSelected = true
            }
        }
    }
    
    func setCell( _ item : PlistItem? ){
        guard let item = item else { return }
        self.item = item
        lblFilterName.text = item.name
        if let frameItem = item as? PlistFrameItem {
            thumbnail.image = frameItem.thumbnailImage
        }else {
            thumbnail.image = item.image
        }
    }

    override var isSelected: Bool {
        willSet {
            super.isSelected = newValue
            lblFilterName?.textColor = newValue ? ColorHelper.selectedMenuColor : ColorHelper.inactiveMenuColor
            guard let item = item else { return }
            
            if let frameItem = item as? PlistFrameItem {
                thumbnail.image = frameItem.thumbnailImage
            }
            else {
                thumbnail?.image = newValue ? item.imageActived : item.image
            }
        }
    }

    func enableButton( _ enabled : Bool ) {
        self.enabled = enabled
        self.lblFilterName?.textColor = enabled ? UIColor.white : UIColor.lightGray
        
        if let item = self.item {
            thumbnail.image = enabled ? item.image : item.imageInActived
        }
        
    }
    
    
}
