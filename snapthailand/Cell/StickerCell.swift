//
//  StickerCell.swift
//  snapthailand
//
//  Created by Nutthawut on 6/13/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit

class StickerCell: UITableViewCell {

    @IBOutlet weak var thumbmain: UIImageView!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBuy: UIButton!
    
    @IBOutlet var thumbnailCollection: [UIImageView]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
