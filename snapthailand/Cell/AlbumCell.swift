//
//  AlbumCell.swift
//  snapthailand
//
//  Created by Nutthawut on 4/27/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit

class AlbumCell: UITableViewCell {

    @IBOutlet weak var lblAlbumName: UILabel!
    @IBOutlet weak var imageAlbum: UIImageView!
    @IBOutlet weak var lblAlbumNum: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
