//
//  FilterStickerCell.swift
//  snapthailand
//
//  Created by Nutthawut on 7/4/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit

class FilterStickerCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbnail: UIImageView!

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}
