//
//  AdjustTableViewCell.swift
//  snapthailand
//
//  Created by Nutthawut on 10/1/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit

class AdjustCollectionCell: UICollectionViewCell {

    @IBOutlet weak var lblTextFont: UILabel!
    @IBOutlet weak var textColorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.defaultStateView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.defaultStateView()
    }

    fileprivate func defaultStateView(){
        textColorView.backgroundColor = UIColor.clear
        textColorView.layer.borderColor = nil
        textColorView.layer.borderWidth = 0
        lblTextFont.text = ""
    }
    
    override var isSelected: Bool {
        willSet {
            self.lblTextFont.textColor = newValue ? ColorHelper.selectedMenuColor : UIColor.white
            super.isSelected = isSelected
        }
    }
}
