//
//  AnswerHelper.swift
//  snapthailand
//
//  Created by Nutthawut on 10/18/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit
import Crashlytics

class AnswerHelper: NSObject {
    
    static func logSelectSticker(_ stickerCollectionName : String) {
        Answers.logCustomEvent(withName: "User's select sticker", customAttributes: ["collecton_name": stickerCollectionName.lowercased()])
    }
    
    static func logSelectFrame(_ frameName : String) {
        Answers.logCustomEvent(withName: "User's select frame", customAttributes: ["frame_name": frameName.lowercased()])
    }

    static func logSelectFilter(_ filterName : String) {
        Answers.logCustomEvent(withName: "User's select filter", customAttributes: ["filter_name": filterName.lowercased()])
    }

    static func logSelectLocationTemplate(_ templateIndex : Int) {
        Answers.logCustomEvent(withName: "User's select location template", customAttributes: ["template": templateIndex])
    }

    static func logStickerBuying(_ pid : String) {
        Answers.logCustomEvent(withName: "User's buy sticker", customAttributes: ["product_id": pid])
    }
    
    static func logShareTo(_ to : AnswerShareTo) {
        Answers.logCustomEvent(withName: "User's share image to", customAttributes: ["method": to.rawValue])
    }

}

enum AnswerShareTo : String {
    case facebook
    case twitter
    case instagram
    case mail
    case cameraroll
    case more
}
