//
//  LocalSticker.swift
//  snapthailand
//
//  Created by Nutthawut on 6/25/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit
import RealmSwift

class LocalSticker: Object {

    dynamic var stickerName : String?
    dynamic var stickerId : String?
    dynamic var stickerFiles : String?
    dynamic var stickerOrder : String?
    dynamic var stickerThumbnailIndex = 0

    func instance() -> LocalStickerInstance{
        let instance = LocalStickerInstance()
        instance.stickerName = stickerName
        instance.stickerId = stickerId
        instance.stickerFiles = stickerFiles
        instance.stickerOrder = stickerOrder
        instance.stickerThumbnailIndex = stickerThumbnailIndex
        return instance
    }
}

class LocalStickerInstance {
    
    var stickerName : String?
    var stickerId : String?
    var stickerFiles : String?
    var stickerOrder : String?
    var stickerThumbnailIndex : Int = 0

    
    func stickerFilesWithPath() -> [String]{
        var stickerFileNameList = [String]()
        
        guard let stickerFiles = stickerFiles else {
            return stickerFileNameList
        }
        
        guard let stickerId = stickerId else {
            return stickerFileNameList
        }
        
        let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        
        for fileName in (stickerFiles as NSString).components(separatedBy: ";") {
            let fileNamePath = ((documentPath as NSString).appendingPathComponent(stickerId) as NSString).appendingPathComponent(fileName)
            
            stickerFileNameList.append(fileNamePath)
        }
        
        return stickerFileNameList
    }
}
