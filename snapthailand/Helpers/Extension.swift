//
//  NTDateFormatter.swift
//  snapthailand
//
//  Created by Nutthawut on 6/11/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit
import Photos

extension Date {    
    func stringFromDateFormatter(_ formatStyle : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatStyle
        return dateFormatter.string(from: self)
    }
}

extension UIImage {
    
    func resizeUserImages() -> UIImage? {
        return self.resize(800, height: 800, compress: 0.95)
    }
    
    func resize (_ width:CGFloat = 60, height:CGFloat = 60, compress: CGFloat = 0.5) -> UIImage? {
        var rect = AVMakeRect(aspectRatio: size, insideRect: CGRect(x: 0, y: 0, width: width, height: height))
        
        rect.size.width = floor(rect.size.width)
        rect.size.height = floor(rect.size.height)
        
        let newSize = rect.size
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.draw(in: CGRect(origin: CGPoint.zero, size: newSize))
        let imageContext = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let image = imageContext else {
            return nil
        }
        
        guard let data = UIImageJPEGRepresentation(image, compress) else {
            return nil
        }
        
        guard let imageFromData = UIImage(data: data) else {
            return nil
        }
        
        let returnImage = UIImage(cgImage: imageFromData.cgImage!, scale: imageFromData.scale, orientation: self.imageOrientation)

        return returnImage
    }
    
    func imageRotatedByDegrees(_ degrees: CGFloat, flip: Bool) -> UIImage {
        
//        let radiansToDegrees: (CGFloat) -> CGFloat = {
//            return $0 * (180.0 / CGFloat(M_PI))
//        }
        let degreesToRadians: (CGFloat) -> CGFloat = {
            return $0 / 180.0 * CGFloat(M_PI)
        }
        
        // calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox = UIView(frame: CGRect(origin: CGPoint.zero, size: size))
        let t = CGAffineTransform(rotationAngle: degreesToRadians(degrees));
        rotatedViewBox.transform = t
        let rotatedSize = rotatedViewBox.frame.size
        
        // Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap = UIGraphicsGetCurrentContext()
        
        // Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap?.translateBy(x: rotatedSize.width / 2.0, y: rotatedSize.height / 2.0);
        
        // Rotate the image context
        bitmap?.rotate(by: degreesToRadians(degrees));
        
        // Now, draw the rotated/scaled image into the context
        var yFlip: CGFloat
        
        if(flip){
            yFlip = CGFloat(-1.0)
        } else {
            yFlip = CGFloat(1.0)
        }
        
        bitmap?.scaleBy(x: yFlip, y: -1.0)
        bitmap?.draw(cgImage!, in: CGRect(x: -size.width / 2, y: -size.height / 2, width: size.width, height: size.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func fixOrientationOfImage() -> UIImage? {
        let image = self
        if image.imageOrientation == .up {
            return image
        }
        
        // We need to calculate the proper transformation to make the image upright.
        // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
        var transform = CGAffineTransform.identity
        
        switch image.imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: image.size.width, y: image.size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: image.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: image.size.height)
            transform = transform.rotated(by: -CGFloat(M_PI_2))
        default:
            break
        }
        
        switch image.imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: image.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: image.size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        default:
            break
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        guard let context = CGContext(data: nil, width: Int(image.size.width), height: Int(image.size.height), bitsPerComponent: (image.cgImage?.bitsPerComponent)!, bytesPerRow: 0, space: (image.cgImage?.colorSpace!)!, bitmapInfo: (image.cgImage?.bitmapInfo.rawValue)!) else {
            return nil
        }
        
        context.concatenate(transform)
        
        switch image.imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            context.draw(image.cgImage!, in: CGRect(x: 0, y: 0, width: image.size.height, height: image.size.width))
        default:
            context.draw(image.cgImage!, in: CGRect(origin: .zero, size: image.size))
        }
        
        // And now we just create a new UIImage from the drawing context
        guard let CGImage = context.makeImage() else {
            return nil
        }
        
        return UIImage(cgImage: CGImage)
    }
}

extension UIViewController {
    
    func showAlert(_ title : String, _ message : String){
        let vc = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionNo = UIAlertAction(title: "Dismiss", style: .cancel){action in}
        vc.addAction(actionNo)
        self.present(vc, animated: true, completion: nil)
    }
}

extension UIColor {
    
    convenience init(r: CGFloat, g: CGFloat, b:CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    
}

extension String {
    
    func getFileName() -> String {
        let fileName = (self as NSString).lastPathComponent
        return fileName
    }
}
