//
//  FilterHelper.swift
//  snapthailand
//
//  Created by Appsynth on 3/29/2559 BE.
//  Copyright © 2559 Nutthawut. All rights reserved.
//

import GPUImage

enum Filter : String {
    case original
    case sepia
    case breakfast
    case snow
    case etikate
    case amatorka
    case soft
    case soft2
    case night
    case lookup1
    case nut
    case nut2
}

struct FilterTemplate {
    var name : String
    var lookupFilter : Filter
}

class FilterHelper: NSObject {

    class func allFilter () -> [Filter]  {
        return [Filter.original, Filter.sepia, Filter.breakfast, Filter.snow]
    }
    
    class func filter(originalImage : UIImage, filter : Filter, intensity : Float, callback:((_ filteredImage : UIImage)->Void)){
        
        guard let path = Bundle.main.path(forResource: filter.rawValue, ofType: "png")
            , let lookupImage = UIImage(contentsOfFile: path) else {
                return
        }
        
        guard let originalImageSource = GPUImagePicture(image: originalImage) else {
            return
        }
        
        guard let lookupImageSource = GPUImagePicture(image: lookupImage) else {
            return
        }
        
        let lookupFilter = GPUImageLookupFilter()
        
        lookupFilter.intensity = CGFloat(intensity)
        originalImageSource.addTarget(lookupFilter)
        lookupImageSource.addTarget(lookupFilter)
        lookupFilter.useNextFrameForImageCapture()
        
        originalImageSource.processImage()
        lookupImageSource.processImage()
        
        guard let filteredImage = lookupFilter.imageFromCurrentFramebuffer(with: originalImage.imageOrientation) else {
            return
        }
        callback(filteredImage)
    
    }

    
    class func configImage(_ filter : Filter?, intensity : CGFloat = 1, contrastLevel : CGFloat, sharpenLevel : CGFloat, brightnessLevel : CGFloat, saturationLevel : CGFloat, originalImage : UIImage, callback:@escaping ((_ filteredImage : UIImage)->Void)){
        
        guard let originalImageSource = GPUImagePicture(image: originalImage) else {
            return
        }

        var lookupFilter : GPUImageLookupFilter?
        var lookupImageSource : GPUImagePicture?
        if let filter = filter
            , let path = Bundle.main.path(forResource: filter.rawValue, ofType: "png")
            , let lookupImage = UIImage(contentsOfFile: path) {
         
            lookupImageSource = GPUImagePicture(image: lookupImage)
            lookupFilter = GPUImageLookupFilter()
            lookupFilter!.intensity = CGFloat(intensity)
        }
        
        let saturationFilter = GPUImageSaturationFilter()
        saturationFilter.saturation = saturationLevel
        let sharpenFilter = GPUImageSharpenFilter()
        sharpenFilter.sharpness = sharpenLevel
        let contrastFilter = GPUImageContrastFilter()
        contrastFilter.contrast = contrastLevel
        let brightnessFilter = GPUImageBrightnessFilter()
        brightnessFilter.brightness = brightnessLevel

        if let lookupFilter = lookupFilter
            , let lookupImageSource = lookupImageSource {
            originalImageSource.addTarget(lookupFilter)
            lookupImageSource.addTarget(lookupFilter)
            lookupFilter.addTarget(saturationFilter)
        } else {
            originalImageSource.addTarget(saturationFilter)
        }
        saturationFilter.addTarget(sharpenFilter)
        sharpenFilter.addTarget(contrastFilter)
        contrastFilter.addTarget(brightnessFilter)
        
        brightnessFilter.useNextFrameForImageCapture()
        
        originalImageSource.processImage()
        
        if let lookupImageSource = lookupImageSource {
            lookupImageSource.processImage()
        }
        
        guard let filteredImage = brightnessFilter.imageFromCurrentFramebuffer(with: originalImage.imageOrientation) else {
            return
        }
        
        callback(filteredImage)
    }
}
