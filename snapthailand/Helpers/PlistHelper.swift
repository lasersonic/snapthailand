//
//  PlistHelper.swift
//  snapthailand
//
//  Created by Nutthawut on 6/12/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit

class PlistHelper: NSObject {
    
    class func loadPlist() -> Plist?{
        if let data = Bundle.main.path(forResource: "data", ofType: "plist") {
        
            if let dictionary = NSDictionary(contentsOfFile: data) {
                //Filter
                var filters = [PlistFilterItem]()
                for filter in dictionary["filter"] as! [[String: String]]{
                    let filterItem = PlistFilterItem()
                    filterItem.name = filter["name"]
                    filterItem.filterName = filter["filterName"]
                    filters.append(filterItem)
                }
                
                //Menu
                var menus = [PlistItem]()
                for plist in dictionary["menu"] as! [[String: String]]{
                    let item = PlistItem()
                    item.name = plist["name"]
                    item.imageName = plist["image"]
                    menus.append(item)
                }
                
                //effect
                var effects = [PlistItem]()
                for plist in dictionary["effect"] as! [[String: String]] {
                    let item = PlistItem()
                    item.name = plist["name"]
                    item.imageName = plist["image"]
                    effects.append(item)
                }
            
                //text
                var texts = [PlistItem]()
                for plist in dictionary["text"] as! [[String: String]] {
                    let item = PlistItem()
                    item.name = plist["name"]
                    item.imageName = plist["image"]
                    texts.append(item)
                }

                //frame
                var frames = [PlistFrameItem]()
                for plist in dictionary["frame"] as! [[String: String]] {
                    let item = PlistFrameItem()
                    item.name = plist["name"]
                    item.imageName = plist["image"]
                    item.color = plist["color"]
                    item.thumbnail = plist["thumbnail"]
                    frames.append(item)
                }
                
                
                //fonts
                var fonts = [PlistFontItem]()
                for plist in dictionary["fonts"] as! [[String: Any]] {
                    let item = PlistFontItem()
                    item.fontname = plist["font-name"] as! String
                    item.example = plist["example"] as! String
                    item.fontsize = plist["font-size"] as! Int
                    item.fontsizeText = plist["font-size-show"] as! Int
                    fonts.append(item)
                }

                let plist = Plist()
                plist.text = texts
                plist.menu = menus
                plist.effect = effects
                plist.filter = filters
                plist.frame = frames
                plist.fonts = fonts
                return plist
            }
        }
        
        return nil
    }

}

class Plist : NSObject {
    var text : [PlistItem]?
    var menu : [PlistItem]?
    var effect : [PlistItem]?
    var filter : [PlistFilterItem]?
    var frame : [PlistFrameItem]?
    var fonts :  [PlistFontItem]?
}

class PlistItem : NSObject {
    var name : String!
    var imageName : String!
    var image : UIImage! {
        return UIImage(named: imageName)
    }
    var imageActived : UIImage! {
        if let activeImage = UIImage(named: imageName+"-active") {
            return activeImage
        }
        return UIImage(named: imageName)
    }
    var imageInActived : UIImage! {
        if let activeImage = UIImage(named: imageName+"-inactive") {
            return activeImage
        }
        return UIImage(named: imageName)
    }
}

class PlistFrameItem : PlistItem {
    
    var color : String!
    var thumbnail : String!
    var thumbnailImage : UIImage! {
        return UIImage(named: thumbnail )
    }
    var getUIColor : UIColor {
        if let color = color {
            let componentColor = color.components(separatedBy: ",")
            if componentColor.count == 3{
                if let r = Int(componentColor[0])
                ,let g = Int(componentColor[1])
                ,let b = Int(componentColor[2]) {
                    return UIColor(red: CGFloat(r), green: CGFloat(g), blue: CGFloat(b), alpha: 1)
                }
            }
        }
        
        return UIColor.clear
    }
}


class PlistFontItem : NSObject {
    var fontname : String!
    var fontsize : Int!
    var example : String!
    var fontsizeText : Int!
    var font : UIFont {
        return UIFont(name: fontname, size: CGFloat(fontsize))!
    }
    var fontText : UIFont {
        return UIFont(name: fontname, size: CGFloat(fontsizeText))!
    }
}

class PlistFilterItem : NSObject {
    var name : String!
    var filterName : String!
    var filterTemplate : FilterTemplate {
        return FilterTemplate(name: name, lookupFilter: Filter(rawValue: filterName)!)
    }
}
