//
//  DownloadManager.swift
//  snapthailand
//
//  Created by Appsynth on 7/6/2559 BE.
//  Copyright © 2559 Nutthawut. All rights reserved.
//

import UIKit
import Alamofire

class DownloadManager: NSObject {

    static var sharedInstance = DownloadManager()
    
    var downloadQueue : [Int]!
    
    override init() {
        downloadQueue = [Int]()
    }
    
    func download( _ sticker : StickerItem, progressBlock : @escaping (_ progress : Float)->Void, successBlock :@escaping ()->Void){

        guard let sticker_id = sticker.sticker_id else {
            return
        }
        
        let requestUrl = String(format:"%@/api/v1/sticker-collections/download", API.baseurl)
        
        let device = UIDevice.current
        
        let data : [AnyHashable: Any] = ["sticker_collection_id": sticker_id, "platform": "ios", "device_id": "DEVICE_ID" , "device_name" : device.name, "os_version" : device.systemVersion]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: [])
            let string = String(data: jsonData, encoding: String.Encoding.utf8)!
            let param = ["data" : string]
            
            downloadQueue.append(sticker_id)
            
            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                let pathComponent = "\(sticker.inapp_id!).zip" //response.suggestedFilename
                let url = directoryURL.appendingPathComponent(pathComponent)
                
                return (url, [.removePreviousFile, .createIntermediateDirectories])
            }
            
            Alamofire.download(requestUrl, method: .post, parameters: param, to: destination)
                .downloadProgress(closure: { progress in
                    print(progress)
                    progressBlock(Float(progress.fractionCompleted))
            })
            .response(completionHandler: { response in
                
                if response.error == nil, let downloadPath = response.destinationURL?.path {

                    if let index = self.downloadQueue.index(of: sticker_id) {
                        self.downloadQueue.remove(at: index)
                    }

                    ZipManager.unzipDownloadedSticker(downloadPath, sticker : sticker, callback: {
                        successBlock()
                    })
                }
            })
        
            
        } catch let error as NSError {
            print(error)
        }
        
    }
    
}
