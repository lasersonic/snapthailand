//
//  ZipManager.swift
//  snapthailand
//
//  Created by Nutthawut on 6/15/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift
import Async
import SwiftKeychainWrapper
import RNCryptor

class ZipManager: NSObject {
        
    class func unzip(_ zipPath : String , destinationPath : String){
        SSZipArchive.unzipFile(atPath: zipPath, toDestination: destinationPath)
    }
    
    class func unzipDownloadedSticker( _ zipFilePath : String , sticker : StickerItem , callback : (()->Void)? = nil ){
        
        let manager = FileManager.default
        let documentFolder = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let fileName = ((zipFilePath as NSString).lastPathComponent as NSString).deletingPathExtension
        let folder = (documentFolder as NSString).appendingPathComponent(fileName)
        
        if SSZipArchive.unzipFile(atPath: zipFilePath, toDestination: folder) {
            
            Async.background {
                let password = KeychainWrapper.standard.string(forKey:KeychainKey)
                let pathItems = try! manager.contentsOfDirectory(atPath: folder)
                
                let filteredPathsItems = pathItems.filter{$0.hasSuffix("png") || $0.hasSuffix("jpg") || $0.hasSuffix("jpeg")}
                
                for itemPath in filteredPathsItems {
                    
                    let eachItemRealPath = folder + "/" + itemPath
                    
                    if let data = try? Data(contentsOf: URL(fileURLWithPath: eachItemRealPath)) {
                        let cypherData = RNCryptor.encrypt(data: data, withPassword: password! )
                        do {
                            try manager.removeItem(atPath: eachItemRealPath)
                            manager.createFile(atPath: eachItemRealPath, contents: cypherData, attributes: nil)
                        }catch {
                            
                        }
                    }
                }                
                
                let localSticker = LocalSticker()
                if let onlyFileNames = sticker.stickerLists?.map({ $0.getFileName() }) {
                    localSticker.stickerFiles = ZipManager.stringArrayToList(onlyFileNames)
                } else {
                    localSticker.stickerFiles = ZipManager.stringArrayToList(filteredPathsItems)
                }
                localSticker.stickerName = sticker.name ?? ""
                localSticker.stickerId = sticker.inapp_id ?? ""
                localSticker.stickerOrder = sticker.orderSequence ?? ""
                localSticker.stickerThumbnailIndex = 0
                
                if let thumbnails = sticker.thumbnails
                    , let avatar_url = sticker.avatar
                    , let index = thumbnails.index(of: avatar_url) {
                    localSticker.stickerThumbnailIndex = index
                }
                
                let realm = try! Realm()
                let query = String(format: "stickerId == '%@'", sticker.inapp_id!)
                let existing = realm.objects(LocalSticker.self).filter(query).count > 0
                if !existing {
                    try! realm.write({
                        realm.add(localSticker)
                    })
                }
                else{
                    let a = realm.objects(LocalSticker.self).filter(query).first
                    print(a ?? "")
                }
                
            }.main {
                let _ = try? manager.removeItem(atPath: zipFilePath)
                callback?()
            }
            
        }

    }
    
    fileprivate class func stringArrayToList(_ lists : [String]) -> String{
        var string = ""
        for l in lists {
            string = (string + ";") + l
        }
        
        let s = string.substring(from: string.characters.index(string.startIndex, offsetBy: 1))
        return s
    }

}
