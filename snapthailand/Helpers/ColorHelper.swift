//
//  ColorHelper.swift
//  snapthailand
//
//  Created by Nutthawut on 8/28/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit

class ColorHelper: NSObject {

    static var selectedMenuColor = UIColor().fromHex(hex: "#00ce5c")
    static var inactiveMenuColor = UIColor.white

    static var progressBarBackgroundColor = UIColor().fromHex(hex: "#AAAAAA")
    static var progressBarProgressColor = UIColor().fromHex(hex: "#00ce5c")
    
}
