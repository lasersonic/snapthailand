//
//  AlbumListViewController.swift
//  snapthailand
//
//  Created by Nutthawut on 4/27/16.
//  Copyright © 2016 Nutthawut. All rights reserved.
//

import UIKit
import Photos

protocol AlbumListDelegate {
    func didSelectCollection(_ name : String, result : PHFetchResult<PHAsset>)
}

class AlbumListViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{

    var delegate : AlbumListDelegate?
    var fetchResults : [PHFetchResult<PHAsset>]!
    var fetchResultsCollection : [PHFetchResult<PHAssetCollection>]!

    override func viewDidLoad() {
        super.viewDidLoad()

        let fetchOptions = PHFetchOptions()

        fetchOptions.sortDescriptors = [
            NSSortDescriptor(key: "creationDate", ascending: false)
        ]
        
        let allPhotoResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        
        let cameraRollResult = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .smartAlbumUserLibrary, options: nil)
        
        let albumResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: nil)

        
        fetchResults = [allPhotoResult]
        fetchResultsCollection = [ cameraRollResult , albumResult  ]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        let count = fetchResultsCollection[section - 1].count
        return count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let count = fetchResults.count + fetchResultsCollection.count
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumCell") as! AlbumCell
        
        if indexPath.section == 0 {
            let num = fetchResults[0].count
            
            cell.lblAlbumName.text = "All Photos"
            cell.lblAlbumNum.text = "\(num)"

            let asset = fetchResults[0][0]
            
            let imageManager = PHCachingImageManager()
            imageManager.requestImage(for: asset,
                                                    targetSize: CGSize(width: 80, height: 80),
                                                    contentMode: .aspectFill,
                                                    options: nil) {
                                                        result, info in
                                                        
                                                            cell.imageAlbum.image = result
                                                        
            }
        }
        
        else {
            
            let album = fetchResultsCollection[indexPath.section - 1][indexPath.row]
            
            cell.lblAlbumName.text = album.localizedTitle
            
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [
                NSSortDescriptor(key: "creationDate", ascending: false)
            ]
            fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
            
            let result = PHAsset.fetchAssets(in: album, options: fetchOptions)
            
            cell.lblAlbumNum.text = "\(result.count)"
            
            result.enumerateObjects(using: { (asset, idx, stop) in
                let imageSize = CGSize(width: 80, height: 80)
                let imageContentMode: PHImageContentMode = .aspectFill
                switch idx {
                case 0:
                    PHCachingImageManager.default().requestImage(for: asset, targetSize: imageSize, contentMode: imageContentMode, options: nil) { (result, _) in
                        cell.imageAlbum.image = result
                    }
                    
                default:
                    // Stop enumeration
                    stop.initialize(to: true)
                }
            })
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            delegate?.didSelectCollection("All Photos", result: fetchResults[0])
        }else{
            let album = fetchResultsCollection[indexPath.section - 1][indexPath.row]
            
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [
                NSSortDescriptor(key: "creationDate", ascending: false)
            ]
            fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
            
            let result = PHAsset.fetchAssets(in: album, options: fetchOptions)
            
            delegate?.didSelectCollection(album.localizedTitle!, result: result)
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
