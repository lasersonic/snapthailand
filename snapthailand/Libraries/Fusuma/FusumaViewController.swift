//
//  FusumaViewController.swift
//  Fusuma
//
//  Created by Yuta Akizuki on 2015/11/14.
//  Copyright © 2015年 ytakzk. All rights reserved.
//

import UIKit
import PhotosUI
import Crashlytics

@objc public protocol FusumaDelegate: class {
    
    func fusumaCameraRollUnauthorized()
    
    @objc optional func fusumaClosed()
    @objc optional func fusumaDismissedWithImage(_ image: UIImage)
}

public var fusumaTintColor       = UIColor.hex("#009688", alpha: 1.0)
public var fusumaBackgroundColor = UIColor.hex("#212121", alpha: 1.0)


public enum FusumaMode {
    case camera
    case library
}

public final class FusumaViewController: UIViewController, FSCameraViewDelegate, FSAlbumViewDelegate {
    
    var willFilter = true

    @IBOutlet weak var photoLibraryViewerContainer: UIView!

    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    
    var albumView  = FSAlbumView.instance()
    
    public weak var delegate: FusumaDelegate? = nil
    
    var albumVC : AlbumListViewController!
    
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!

    override public func loadView() {
        
        if let view = UINib(nibName: "FusumaViewController", bundle: Bundle(for: self.classForCoder)).instantiate(withOwner: self, options: nil).first as? UIView {
            self.view = view
        }
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    
        self.view.backgroundColor = fusumaBackgroundColor
        
        albumView.delegate  = self

        menuView.backgroundColor = fusumaBackgroundColor
        menuView.addBottomBorder(UIColor.black, width: 1.0)
        
        photoLibraryViewerContainer.addSubview(albumView)
        
        albumView.frame  = CGRect(origin: CGPoint.zero, size: photoLibraryViewerContainer.frame.size)
        albumView.layoutIfNeeded()
        albumView.initialize()
        
    }
    
    func addAlbum(){
        
        if albumVC != nil {
            return
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        albumVC = storyboard.instantiateViewController(withIdentifier: "AlbumListViewController") as! AlbumListViewController
        albumVC.delegate = self

        self.addChildViewController(albumVC)
        albumVC.didMove(toParentViewController: self)
        self.view.addSubview(albumVC.view)
        albumVC.view.frame = self.view.frame
        self.albumVC.view.center.y = 1200

    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    override public var prefersStatusBarHidden : Bool {
        return true
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {

        self.dismiss(animated: true, completion: {
            
            self.delegate?.fusumaClosed?()
        })
    }
    
    // MARK: FSCameraViewDelegate
    func cameraShotFinished(_ image: UIImage) {
        
        self.dismiss(animated: true, completion: {
        
            self.delegate?.fusumaDismissedWithImage?(image)
        })
    }
    
    // MARK: FSAlbumViewDelegate
    public func albumViewCameraRollUnauthorized() {
        delegate?.fusumaCameraRollUnauthorized()
        
        let alertController = UIAlertController (title: "Permission Needed", message: "For using this function camera / gallery permission is needed.", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.openURL(settingsUrl)
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    public func didUserChooseImage(_ image: UIImage) {

        Answers.logCustomEvent(withName: "User's image source", customAttributes: ["source":"library"])

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let filterVC = storyboard.instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController {
            filterVC.image = image.resizeUserImages()
            self.navigationController?.pushViewController(filterVC, animated: true)
        }
    }
    
    deinit {
        print("close fusuma")
    }
    
    @IBAction func albumAction(_ sender: AnyObject) {
        addAlbum()
        toggleAlbum()
    }
    
    func toggleAlbum() {
        
        var y : CGFloat = 1200
        var angle : CGFloat = 0
        
        if self.albumVC.view.center.y == 1200 {
            y = self.view.center.y + 44
            angle = -180
        }

        UIView.animate(withDuration: 0.3, animations: {
            self.imgArrow.transform = CGAffineTransform(rotationAngle: (angle * CGFloat(M_PI)) / 180.0)
            self.albumVC.view.center.y = y
            self.hideCloseDoneButton( y != 1200)
        }) 
    }
    
    func hideCloseDoneButton( _ hide : Bool ){
        self.closeButton.alpha = hide ? 0 : 1
    }
}

extension FusumaViewController : AlbumListDelegate {
    
    func didSelectCollection(_ name : String, result : PHFetchResult<PHAsset>) {
        self.lblTitle.text = name.uppercased()
        
        UIView.animate(withDuration: 0.3, animations: {
            self.imgArrow.transform = CGAffineTransform(rotationAngle: (0 * CGFloat(M_PI)) / 180.0)
        })
        
        albumView.images = result
        toggleAlbum()
    }
}
 
